# Le PIAF documentation

The goal of this is to describe the structure of the events used by Le PIAF. This, in order to help for contributors and if someone wants to build a compatible application.

The main data structure is `fr.emse.minitel.asso`, a room representing an association. Here, an association is a group of person, an organisation which can propose events, news to his members. Also, we should add a way for users to know the hierachy of the association.


## Portail [space]

Because the main target of this application is schools, we need to have space which will contains all the associations of the school. Thus the need of a portail room.

```json
{
    "type" : "m.room.type",
    "content" : {
        "type" : "fr.emse.minitel.portail"
    }
}
```

**//NB:** This is a very specific structure. The application should be able to display all the associations even if the portail hasn't been found. 


### User information [state]

The portal can also be sued to store personal informations of the members of the school. This will allow to display a list of all the members of the school and let them add information visible only to members of the schools

```
fr.emse.minitel.user
```

State:

```json
{
    "key": <matrix user id>,
    "content":{
        "birthday": timestamp,
        "promo": "EI20", // should be an enum
        "about": "some random text about me",
        "site": "https://my_awesome_website.swag"
    }
}
```

## Association (aka Association as Space)

This is the main structure of the association.

It's a `m.space` in order to have sub rooms. This in order to store sub rooms like events room, store rooms or items room (more about this later).

Thus association room inherit from the spaces property (m.room.create, m.space.child...).

Assocation description :

* name -> `m.room.name`
* about -> `m.room.topic` *What about Markdown ?*

```json
{
    "type" : "m.room.type",
    "content" : {
        "type" : "fr.emse.minitel.asso"
    }
}
```

### Hierarchy [state]

We need to define the organisatino structure. However, we need to let the user define his own organisation for his organisation.

A user may have multiple roles (here positions).

```json
{
    "type": "fr.emse.minitel.members",
    "state_key": "@the_user_matrix_id"
    "content": {
        "positions": [
            "fr.emse.minitel.president"
            <the others positions id>
        ]
    }
}
```

### Position [state]

In order to let the user define it's custom position, we define this custom state to store the postions.

```json
{
    "type": "fr.emse.minitel.positions",
    "state_key": "position_name"
    "content": {
        "name": "The name of the role",
        "description": "It's description"
    }
}
```

We define a list of custom position that will automaticaly be added on room creation to speed up the association configuration.

Default position are :

```
* fr.emse.minitel.president
* fr.emse.minitel.vice_president
* fr.emse.minitel.tresorier
* fr.emse.minitel.secretaire_general
* fr.emse.minitel.cotisant
```

### Posts [event] (aka News event)

We want to be able to send news to the members of the associations. Thus we will use the `post` event as described 
It's useful to be able to send news.

Post event is described in [MSC3639 - Matrix for the social media use case](https://github.com/matrix-org/matrix-doc/pull/3639)

**WARNING** : News event power level should be different from 0 non moderator user to send news.

## Event [room]

see [event_description](event_description.md) for the detailled proposal.

## Store [room]

```
fr.emse.minitel.portail.store
```

A room to store soter items that members of the association can buy.

**//TODO** -- Not yet implemented