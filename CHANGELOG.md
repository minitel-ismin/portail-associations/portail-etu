# Change Log for le PIAF

## 1.3.4

Let’s update the app to the latest version of the widget sdk.

## Unreleased

### Chore
* update dep [0c4575d]

### Feature
* reload news page [d229f27]

### Fixes
* format [1c20f30]

## v1.1.8 - 2021-12-08

### Feature
* made user asso list clickable [d43d53d]
* added navigation search [798c1f3]

### Fixes
* get user info [77f201d]
* loading bar [aa9190f]
* web login [9f97dbc]
* loggin [85f3315]

## v1.1.7 - 2021-12-08

### Feature
* update [62036fb]

### Fixes
* date time [f0de0b2]
* removed unused partials [a6046f3]
* reverse list [8741d25]
* don't display edits [925b82e]
* room permissions [5c7702d]
* name [b823310]
* navigation [b6d59ce]

## v1.1.6 - 2021-12-07

### Feature
* store item creation [30426ae]
* widgetize event admin zone [dc5dc98]

### Fixes
* navigation [df5ab12]
* navigation update [bf4c278]
* display reservation confirmation [4e03755]
* make markdown links clickable [0a5c51a]
* format [07b0173]
* loading bar [9ea5847]

## v1.1.5 - 2021-12-07

### Feature
* update dependencies [5f7f901]
* load data [410e71b]
* cache matrix rooms [d59d073]

### Fixes
* db [c7dc7ac]
* don't throw error when can't access mediaquery [0747f57]

## v1.1.3 - 2021-12-06

### Chore
* dependencies update [3715d98]

### Feature
* auto join rooms [999d8a3]
* adding version number [240a986]

## v1.1.2 - 2021-12-05

### Feature
* add padding on mobile [c5abe9a]
* wait before continue [8c5bd2f]

### Fixes
* formating [9dce0d7]
* wait for client loading [2105a5d]
* loading [3cf034f]
* null check [bac110b]
* check if room is a space [b953a07]

## v1.1.1 - 2021-12-04

### Feature
* try to optimize post loading [d59cc9a]
* allow admin to create room [bd1aa7a]
* allow settings news room settings [2e163f2]
* request history and changed method names [7918b56]
* automaticaly join all rooms [fe09aca]
* added a button to join all rooms [483b674]
* display store reservations [cb26570]
* allow deleting item [4aad0bf]
* allow deleting rentals and store items [0ae4475]
* new navigation [278dfce]
* added debug [7d2a155]

### Fixes
* formating [0787f30]
* make sure we try to login [9872233]
* doesn't allow to login via web on mobile [4fc3f3c]
* open links in the same page [0ace71e]
* removed duplicate navigation [00d1310]
* double navigation [b7a7ec5]
* remove old page [179e23d]
* prevent calling setState when unmounted [4178170]
* move to custom header [6532777]
* side menu naviqgation [615562a]
* navigation [5771da7]
* too much loading [f078d12]
* appbars [65fbe84]
* update version code [73895b4]
* unused improts [5e47c01]
* output [f2f85c7]
* unused import [629381d]

### Refactor
* moved bookings files [2e76bba]

## v1.1.0 - 2021-12-04

### Feature
* update event page [b9aa874]

## v0.0.16 - 2021-12-04

### Feature
* enhance view [a977650]
* add news room join button [9d4af7a]
* speed up users loading [7ec0fbd]
* correct login [f06bac6]
* adedd support for web login [1bf2da0]
* update CI [5a4143e]
* added cost [8457098]
* redisigned reservations [e14b9ac]
* display update on dashboard [304afbe]
* migrated navigator [3ef84b4]
* migrate to sembast [0605b2b]
* migrate to null safety [51aeaf2]
* enable remote notifications [3086d61]
* update build options [3706f07]

### Fixes
* null assumption [6b11995]
* hide details when not bookable [e04b7b6]
* android builds [5043fd4]
* dark mode [b4b3d22]
* navigation [7346952]
* mobile navigation [4ce8d77]
* event loading [28d52ef]
* reservation item color [cd817aa]
* format [d5261bb]
* resolve linux build issue [ba57617]
* added back code signing [5c838d3]
* navigation [832736d]
* navigation on mobile [6a7a357]
* format [8e829b2]
* alginement [858d2f7]
* removed unused imports [0bce48d]
* code cleanup [60d8888]
* removed ICM [6613f09]


This CHANGELOG.md was generated with [**Changelog for Dart**](https://pub.dartlang.org/packages/changelog)
