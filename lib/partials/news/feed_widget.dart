import 'package:emse_bde_app/pages/feed_page.dart';
import 'package:emse_bde_app/partials/helpers/text.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/config/matrix_types.dart';
import 'package:minestrix_chat/partials/feed/posts/matrix_post_editor.dart';
import 'package:minestrix_chat/partials/social/social_gallery_preview_widget.dart';

/// A class to display a collection of posts from a specifict room.
/// WARNING: This widget doesn't implement post auto reload on client.onSync
class MatrixPostsWidget extends StatelessWidget {
  final Room room;
  const MatrixPostsWidget({Key? key, required this.room}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Timeline>(
        future: room.getTimeline(),
        builder: (context, timelineSnap) {
          print("Debug : rebuild MatrixPostsWidget timeline");
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              H2Title("Gallerie"),
              if (timelineSnap.hasData)
                SocialGalleryPreviewWigdet(
                    room: room, timeline: timelineSnap.data!),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  H2Title("Actualitées"),
                  if (room.canSendEvent(MatrixTypes.post))
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8.0, horizontal: 14),
                      child: IconButton(
                          icon: Icon(Icons.playlist_add_rounded),
                          onPressed: () async {
                            await showDialog(
                                context: context,
                                builder: (context) => Dialog(
                                      child: ConstrainedBox(
                                        constraints: BoxConstraints(
                                            maxWidth: 800, maxHeight: 800),
                                        child: PostEditorPage(
                                          room: [room],
                                        ),
                                      ),
                                    ));
                          }),
                    ),
                ],
              ),
              FeedView(room: room)
            ],
          );
        });
  }
}
