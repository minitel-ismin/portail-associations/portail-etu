import 'package:auto_route/src/router/auto_router_x.dart';

import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/material.dart';
import 'package:minestrix_chat/partials/sync/sync_status_card.dart';

class SideMenu extends StatelessWidget {
  Widget sideMenuButton(
      BuildContext context, String text, Icon icon, Function()? onPressed) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: MaterialButton(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                icon,
                SizedBox(
                  width: 6,
                ),
                Flexible(
                  child: Text(
                    text,
                    style: TextStyle(
                        color: Theme.of(context).textTheme.bodyText1!.color,
                        fontSize: 18,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ],
            ),
          ),
          onPressed: onPressed),
    );
  }

  @override
  Widget build(BuildContext context) {
    final client = Matrix.of(context).client;
    return Container(
      color: Theme.of(context).cardColor,
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Le PIAF",
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
            ),
          ),
          Flexible(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                sideMenuButton(
                    context, "Actualitées", Icon(Icons.fiber_new_sharp), () {
                  context.navigateTo(FeedRoute());
                  if (Navigator.canPop(context)) context.router.pop();
                }),
                sideMenuButton(context, "Locations", Icon(Icons.key), () {
                  context.navigateTo(RentalsRoute());
                  if (Navigator.canPop(context)) context.router.pop();
                }),
                sideMenuButton(context, "Store", Icon(Icons.store), () {
                  context.navigateTo(StoreRoute());
                  if (Navigator.canPop(context)) context.router.pop();
                }),
                sideMenuButton(context, "Utilisateurs", Icon(Icons.people), () {
                  context.navigateTo(UserListRoute());
                  if (Navigator.canPop(context)) context.router.pop();
                }),
              ],
            ),
          ),
          SyncStatusCard(client: client),
        ],
      ),
    );
  }
}
