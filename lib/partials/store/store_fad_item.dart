import 'dart:ui' as ui;

import 'package:flutter/material.dart';

class StoreFadButton extends StatefulWidget {
  const StoreFadButton(
      {Key? key,
      this.onListPressed,
      this.onEditPressed,
      this.onBuyPressed,
      this.onDetailPressed,
      this.child,
      this.canEdit})
      : super(key: key);

  final VoidCallback? onListPressed;
  final VoidCallback? onEditPressed;
  final VoidCallback? onBuyPressed;
  final VoidCallback? onDetailPressed;

  final Widget? child;
  final bool? canEdit;

  @override
  _StoreFadButtonState createState() => _StoreFadButtonState();
}

class _StoreFadButtonState extends State<StoreFadButton> {
  bool _focused = false;
  bool _hovering = false;

  Color get color {
    Color baseColor = Colors.lightBlue;
    if (_focused) {
      baseColor = Color.alphaBlend(Colors.black.withOpacity(0.25), baseColor);
    }
    if (_hovering) {
      baseColor = Color.alphaBlend(Colors.black.withOpacity(0.1), baseColor);
    }
    return baseColor;
  }

  void _handleFocusHighlight(bool value) {
    setState(() {
      _focused = value;
    });
  }

  void _handleHoveHighlight(bool value) {
    setState(() {
      _hovering = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return FocusableActionDetector(
      onShowFocusHighlight: _handleFocusHighlight,
      onShowHoverHighlight: _handleHoveHighlight,
      child: Stack(
        children: <Widget>[
          widget.child!,
          if (_hovering)
            ClipRRect(
              child: BackdropFilter(
                filter: ui.ImageFilter.blur(
                  sigmaX: 1.0,
                  sigmaY: 1.0,
                ),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.black12,
                      borderRadius: BorderRadius.all(Radius.circular(12))),
                ),
              ),
            ),
          if (_hovering && widget.canEdit!)
            Positioned(
                top: 15,
                right: 15,
                child: Row(
                  children: [
                    IconButton(
                        icon: Icon(Icons.list, color: Colors.white),
                        onPressed: widget.onListPressed),
                    IconButton(
                        icon: Icon(Icons.edit, color: Colors.white),
                        onPressed: widget.onEditPressed),
                  ],
                )),
          if (_hovering)
            Center(
                child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                      child: Text("Acheter"), onPressed: widget.onBuyPressed),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: MaterialButton(
                      color: Colors.white,
                      child: Text("A propos",
                          style: TextStyle(color: Colors.black)),
                      onPressed: widget.onDetailPressed),
                )
              ],
            )),
        ],
      ),
    );
  }
}
