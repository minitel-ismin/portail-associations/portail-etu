import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  CustomTextField(
      {Key? key,
      this.name,
      this.onChanged,
      this.text,
      this.minLines,
      this.maxLines = 1,
      this.number = false,
      this.color})
      : super(key: key);
  final String? name;
  final Function? onChanged;
  final String? text;
  final int? minLines;
  final int? maxLines;
  final bool number;
  final Color? color;

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  TextEditingController? ed;

  @override
  Widget build(BuildContext context) {
    if (ed == null) {
      ed = TextEditingController(text: widget.text);
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              onEditingComplete: () {
                if (widget.number) {
                  double? value = double.tryParse(ed!.text);
                  if (value == null) {
                    value = 0;
                    setState(() {
                      ed!.text = value.toString();
                    });
                  }
                  widget.onChanged!(value);
                }
              },
              onChanged: (sValue) {
                if (widget.number) {
                  double? value = double.tryParse(ed!.text);
                  if (value == null) {
                    value = 0;
                  }
                  widget.onChanged!(value);
                } else
                  widget.onChanged!(sValue);
              },
              cursorColor: widget.color,
              controller: ed,
              minLines: widget.minLines,
              maxLines: widget.maxLines,
              style: TextStyle(color: widget.color),
              decoration: InputDecoration(
                labelStyle: TextStyle(color: widget.color),
                labelText: widget.name,
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: widget.color != null
                          ? widget.color!
                          : Theme.of(context).colorScheme.secondary,
                      width: 2.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: widget.color != null
                          ? widget.color!
                          : Theme.of(context).colorScheme.secondary,
                      width: 2.0),
                ),
              ),
              keyboardType: widget.number
                  ? TextInputType.numberWithOptions(decimal: true)
                  : widget.maxLines != 1
                      ? TextInputType.multiline
                      : TextInputType.text,
              textInputAction: TextInputAction.newline,
            ),
          ),
          if (widget.number)
            Column(
              children: [
                IconButton(
                    icon: Icon(Icons.arrow_upward, color: widget.color),
                    onPressed: () {
                      double? value = double.tryParse(ed!.text);
                      if (value == null) value = 0;
                      value += 1;

                      widget.onChanged!(value);

                      setState(() {
                        ed!.text = value.toString();
                      });
                    }),
                IconButton(
                    icon: Icon(Icons.arrow_downward, color: widget.color),
                    onPressed: () {
                      double? value = double.tryParse(ed!.text);
                      if (value == null) value = 0;
                      if (value > 0) value -= 1;

                      widget.onChanged!(value);

                      setState(() {
                        ed!.text = value.toString();
                      });
                    }),
              ],
            )
        ],
      ),
    );
  }
}
