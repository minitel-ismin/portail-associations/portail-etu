import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

/// Custom button. When pressed, call onPressed or onFuturePressed.
/// When onFuturePressed is non null, it will display a loading progress indicator
class CustomElevatedButton extends StatefulWidget {
  const CustomElevatedButton(
      {required this.message,
      this.onPressed,
      this.onFuturePressed,
      required this.icon,
      Key? key,
      this.color,
      this.minimized = true,
      this.updating = false})
      : assert(!(onPressed == null && updating == true)),
        super(key: key);

  final Function? onPressed;
  final AsyncCallback? onFuturePressed;
  final String message;
  final IconData icon;
  final Color? color;
  final bool minimized;
  final bool updating;

  @override
  _CustomElevatedButtonState createState() => _CustomElevatedButtonState();
}

class _CustomElevatedButtonState extends State<CustomElevatedButton> {
  bool _updating = false;

  bool get updating =>
      widget.onFuturePressed == null ? widget.updating : _updating;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(primary: widget.color),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              mainAxisSize:
                  widget.minimized ? MainAxisSize.min : MainAxisSize.max,
              children: [
                if (!updating) Icon(widget.icon),
                if (updating) CircularProgressIndicator(color: Colors.white),
                SizedBox(width: 12),
                Text(widget.message),
              ],
            ),
          ),
          onPressed: widget.onPressed == null && widget.onFuturePressed == null
              ? null
              : () async {
                  if (!updating) {
                    if (widget.onPressed != null) {
                      widget.onPressed!();
                    } else {
                      setState(() {
                        _updating = true;
                      });
                      try {
                        await widget.onFuturePressed!();
                      } finally {
                        setState(() {
                          _updating = false;
                        });
                      }
                    }
                  }
                }),
    );
  }
}
