import 'package:drag_and_drop_lists/drag_and_drop_lists.dart';
import 'package:emse_bde_app/logic/models/json/event/booking_question.dart';
import 'package:emse_bde_app/logic/models/json/event/portail_event_booking.dart';
import 'package:emse_bde_app/partials/controls/custom_elevetated_button.dart';
import 'package:emse_bde_app/partials/controls/custom_text_field.dart';
import 'package:flutter/material.dart';

class EventFormCreator extends StatefulWidget {
  final Function(PortailEventBooking p1) onChanged;
  final PortailEventBooking booking;

  const EventFormCreator(
      {Key? key, required this.booking, required this.onChanged})
      : super(key: key);

  @override
  _EventFormCreatorState createState() => _EventFormCreatorState();
}

class _EventFormCreatorState extends State<EventFormCreator> {
  late PortailEventBooking booking;
  @override
  void initState() {
    super.initState();
    booking = widget.booking;
  }

  List<DragAndDropList>? _contents;

  void generateDragAndDropListFromOptions() {
    if (_contents == null)
      _contents = [];
    else
      _contents!.clear();

    for (BookingQuestion option in booking.form_questions) {
      _contents!.add(getDragAndDropListFromOption(option));
    }
  }

  DragAndDropList getDragAndDropListFromOption(BookingQuestion option) {
    TextEditingController controller = TextEditingController(text: option.id);

    DragAndDropList item = DragAndDropList(
      header: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              IconButton(
                  onPressed: () {
                    setState(() {
                      booking.form_questions.remove(option);
                    });

                    generateDragAndDropListFromOptions();
                    setState(() {});
                  },
                  icon: Icon(Icons.delete)),
              Text("Champs : " + OptionTypesNames[option.type]!,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
            ],
          ),
          SizedBox(height: 12),
          Row(
            children: [
              Expanded(
                child: TextField(
                    controller: controller,
                    decoration: InputDecoration(
                        labelText: "Titre", border: const OutlineInputBorder()),
                    onChanged: (String text) {
                      option.id = text;
                    }),
              ),
              SizedBox(width: 20),
              DropdownButton<QuestionType>(
                value: option.type,
                items: QuestionType.values
                    .map<DropdownMenuItem<QuestionType>>((QuestionType value) {
                  return DropdownMenuItem<QuestionType>(
                    value: value,
                    child: Text(OptionTypesNames[value]!),
                  );
                }).toList(),
                onChanged: (QuestionType? value) {
                  if (value != null) {
                    option.type = value;
                    generateDragAndDropListFromOptions();
                    setState(() {});
                  }
                },
              ),
            ],
          ),
          if (option.type != QuestionType.title)
            CheckboxListTile(
                value: option.optional,
                title: Text("Champs requis ?"),
                subtitle: !option.optional
                    ? Text(
                        "Les participants devront obligatoirement remplir ce champs")
                    : Text(
                        "Les participants NE devront PAS obligatoirement remplir ce champs"),
                onChanged: (bool? val) {
                  if (val != null)
                    setState(() {
                      option.optional = val;
                    });
                }),
          if (option.type == QuestionType.choice)
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Ajouter une option"),
                  ),
                  onPressed: () {
                    int id = 0;

                    // find a non used id
                    while (option.answers.indexWhere((e) => e.id == id) != -1)
                      id++;

                    BookingQuestionAnswers opt = BookingQuestionAnswers();
                    opt.id = id.toString();
                    opt.cost = 0;
                    opt.text = "";

                    setState(() {
                      option.answers.add(opt);
                    });
                  }),
            ),
        ],
      ),
      contentsWhenEmpty: Container(),
      children: <DragAndDropItem>[
        for (var formOption in option.answers)
          DragAndDropItem(
            child: Row(
              children: [
                IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () {
                      option.answers.remove(formOption);
                      setState(() {});
                    }),
                Expanded(
                  child: TextField(
                      controller: TextEditingController(text: formOption.text),
                      decoration: InputDecoration(
                          labelText: "Nom", border: const OutlineInputBorder()),
                      onChanged: (String text) {
                        formOption.text = text;
                      }),
                ),
                Expanded(
                  child: CustomTextField(
                      name: "Price",
                      text: formOption.cost.toString(),
                      number: true,
                      onChanged: (value) {
                        formOption.cost = value;
                      }),
                ),
              ],
            ),
          ),
      ],
    );

    return item;
  }
  // list

  _onItemReorder(
      int oldItemIndex, int oldListIndex, int newItemIndex, int newListIndex) {
    if (oldListIndex == newListIndex) {
      var movedItem =
          booking.form_questions[oldListIndex].answers.removeAt(oldItemIndex);
      booking.form_questions[newListIndex].answers
          .insert(newItemIndex, movedItem);
      setState(() {
        var movedItemContent =
            _contents![oldListIndex].children.removeAt(oldItemIndex);
        _contents![newListIndex]
            .children
            .insert(newItemIndex, movedItemContent);
      });
    }
  }

  _onListReorder(int oldListIndex, int newListIndex) {
    var movedList = booking.form_questions.removeAt(oldListIndex);
    booking.form_questions.insert(newListIndex, movedList);
    setState(() {
      var movedListContent = _contents!.removeAt(oldListIndex);
      _contents!.insert(newListIndex, movedListContent);
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_contents == null) _contents = [];
    generateDragAndDropListFromOptions();

    return DragAndDropLists(
        children: _contents!,
        onItemReorder: _onItemReorder,
        onListReorder: _onListReorder,
        disableScrolling: true,
        listPadding: EdgeInsets.symmetric(horizontal: 15, vertical: 4),
        listDragHandle: DragHandle(
          verticalAlignment: DragHandleVerticalAlignment.top,
          child: Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Icon(
              Icons.menu,
            ),
          ),
        ),
        itemDragHandle: DragHandle(
          child: Icon(Icons.menu),
        ),
        contentsWhenEmpty: CustomElevatedButton(
            message: "Créer un formulaire ?",
            icon: Icons.add,
            onPressed: () {
              BookingQuestion question = BookingQuestion();
              booking.form_questions.add(question);

              generateDragAndDropListFromOptions();
              setState(() {});
            }));
  }
}
