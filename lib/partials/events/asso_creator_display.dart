import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';

import '../../logic/models/association_data.dart';
import '../../router.gr.dart';
import '../asso/matrix_asso_list_item.dart';

class AssoCreatorDisplay extends StatelessWidget {
  const AssoCreatorDisplay({Key? key, required this.room}) : super(key: key);

  final Room room;
  @override
  Widget build(BuildContext context) {
    final rooms = room.states["m.space.parent"]?.values.toList().map((state) {
          final url = state.stateKey;
          if ((state.content.tryGetList('via')?.isEmpty ?? true) || url == null)
            return null;
          return room.client.getRoomById(url);
        }).where((element) => element != null) ??
        [];
    return rooms.isNotEmpty
        ? Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0),
              child: Text("Assos organisatrices",
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.w500)),
            ),
            for (final assoRoom in rooms)
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: MatrixAssoListItem(
                    assos: assoRoom!,
                    onPressed: () {
                      AssociationData d = AssociationData.fromRoom(assoRoom);
                      context.navigateTo(AssoWrapperRoute(
                          children: [AssociationRoute(assoData: d)]));
                    }),
              )
          ])
        : Container();
  }
}
