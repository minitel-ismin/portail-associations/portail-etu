import 'package:flutter/material.dart';

class DateSelector extends StatefulWidget {
  final DateTime? date;
  final Function onNewDate;
  final String name;
  final DateTime? firstDate;
  final DateTime? lastDate;
  DateSelector(
      {this.date,
      required this.onNewDate,
      this.name = "SELECT DATE TIME",
      this.firstDate,
      this.lastDate});

  @override
  _DateSelectorState createState() => _DateSelectorState();
}

class _DateSelectorState extends State<DateSelector> {
  final double radius = 6;
  DateTime? _date;

  DateTime get d => _date ?? DateTime.now();
  String get dateAsString =>
      d.day.toString().padLeft(2, "0") +
      "/" +
      d.month.toString().padLeft(2, "0") +
      "/" +
      d.year.toString();

  String get timeAsString =>
      d.hour.toString().padLeft(2, "0") +
      " : " +
      d.minute.toString().padLeft(2, "0");

  @override
  void initState() {
    super.initState();
    _date ??= widget.date;
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
        leading: Icon(Icons.timeline),
        title: Text(widget.name),
        subtitle: _date == null
            ? Text("Set date")
            : Text(dateAsString + " " + timeAsString),
        trailing: Icon(Icons.edit),
        onTap: () async {
          await showDialog(
              context: context,
              builder: (context) => Dialog(
                      child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(radius)),
                    elevation: 18,
                    color: Colors.blue,
                    child: IntrinsicHeight(
                      child: Column(
                        children: render(context, d),
                      ),
                    ),
                  )));
        });
  }

  List<Widget> render(context, d) {
    return [
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 18.0, horizontal: 18),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(widget.name,
                    style: TextStyle(
                        color: Colors.white,
                        letterSpacing: 1.2,
                        fontSize: 14,
                        fontWeight: FontWeight.w400)),
                SizedBox(height: 10),
                Row(
                  children: [
                    Text(dateAsString,
                        style: TextStyle(fontSize: 26, color: Colors.white)),
                  ],
                ),
                SizedBox(height: 4),
                Row(
                  children: [
                    Text(timeAsString,
                        style: TextStyle(fontSize: 26, color: Colors.white)),
                  ],
                ),
              ],
            ),
            Center(
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                  onPrimary: Colors.blue,
                ),
                onPressed: () async {
                  TimeOfDay? t = await showTimePicker(
                      context: context,
                      initialTime: TimeOfDay(hour: d.hour, minute: d.minute));
                  if (t != null) {
                    _date = d.add(Duration(
                        hours: t.hour - d.hour as int,
                        minutes: t.minute - d.minute as int));
                    setState(() {});
                  }
                },
                child: Text("Choisir l'heure"),
              ),
            )
          ],
        ),
      ),
      Expanded(
        child: Container(
          decoration: BoxDecoration(
              color: Theme.of(context).scaffoldBackgroundColor,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(radius),
                  bottomRight: Radius.circular(radius))),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              CalendarDatePicker(
                  initialDate: d,
                  firstDate: d.millisecondsSinceEpoch <
                          DateTime.now().millisecondsSinceEpoch
                      ? d
                      : widget.firstDate != null
                          ? widget.firstDate
                          : DateTime.now(),
                  lastDate: widget.lastDate != null
                      ? widget.lastDate!
                      : DateTime.now().add(Duration(days: 365)),
                  onDateChanged: (newDate) {
                    _date =
                        newDate.add(Duration(hours: d.hour, minutes: d.minute));
                    setState(() {});
                  }),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 18.0, vertical: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                        child: Text("RESET", style: TextStyle(fontSize: 15)),
                        onPressed: () {
                          _date = widget.date;
                          setState(() {});
                        }),
                    TextButton(
                        child: Text("OK", style: TextStyle(fontSize: 15)),
                        onPressed: () {
                          widget.onNewDate(_date);
                          Navigator.of(context).pop();
                        })
                  ],
                ),
              ),
            ],
          ),
        ),
      )
    ];
  }
}
