import 'package:flutter/material.dart';

class PortailTitle extends StatelessWidget {
  const PortailTitle(this.title);
  final String title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(title,
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600)),
    );
  }
}

class PortailH2 extends StatelessWidget {
  const PortailH2(this.title, {this.color});
  final String? title;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        title!,
        overflow: TextOverflow.clip,
        style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w700,
            letterSpacing: 1.15,
            color: color),
      ),
    );
  }
}
