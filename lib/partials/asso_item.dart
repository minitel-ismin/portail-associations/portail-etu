import 'package:flutter/material.dart';

class AssoItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: MaterialButton(
          color: Colors.red[400],
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(12)),
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                Text("MINITEL",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 30,
                        fontWeight: FontWeight.normal)),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Icon(Icons.computer, color: Colors.white, size: 80),
                ),
                TextButton(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("En savoirs plus",
                        style: TextStyle(color: Colors.white)),
                  ),
                  onPressed: () {},
                  style: ButtonStyle(
                    side: MaterialStateProperty.all(
                      BorderSide(width: 2, color: Colors.white),
                    ),
                  ),
                )
              ],
            ),
          ),
          onPressed: () {}),
    );
  }
}
