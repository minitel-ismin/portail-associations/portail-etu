import 'package:auto_route/src/router/auto_router_x.dart';

import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:emse_bde_app/logic/models/json/user/portailUser.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/partials/matrix/matrix_image_avatar.dart';

class UserListItem extends StatefulWidget {
  final PortailUser user;
  const UserListItem({Key? key, required this.user}) : super(key: key);

  @override
  _UserListItemState createState() => _UserListItemState();
}

class _UserListItemState extends State<UserListItem> {
  @override
  Widget build(BuildContext context) {
    final m = Matrix.of(context).client;

    return FutureBuilder<ProfileInformation>(
        future: m.getUserProfile(widget.user.userID),
        builder: (context, snap) {
          return ListTile(
              title: Text(snap.data?.displayname ?? widget.user.userID),
              subtitle: widget.user.promo == null
                  ? null
                  : Text(widget.user.promo.toString()),
              leading: MatrixImageAvatar(
                client: Matrix.of(context).client,
                height: 40,
                width: 40,
                defaultText: snap.data?.displayname,
                backgroundColor: Colors.blue,
                url: snap.data?.avatarUrl,
                fit: true,
              ),
              onTap: () async {
                context.router.push(UserDetailRoute(user: widget.user));
              });
        });
  }
}
