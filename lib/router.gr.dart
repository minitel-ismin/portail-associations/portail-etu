// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i42;
import 'package:auto_route/empty_router_widgets.dart' as _i2;
import 'package:flutter/material.dart' as _i43;
import 'package:matrix/matrix.dart' as _i48;
import 'package:minestrix_chat/view/room_list/room_list_room.dart' as _i38;
import 'package:minestrix_chat/view/room_list/room_list_space.dart' as _i37;

import 'logic/models/association_data.dart' as _i51;
import 'logic/models/event/base_portail_event.dart' as _i45;
import 'logic/models/event/portail_event.dart' as _i46;
import 'logic/models/event/rental_item.dart' as _i49;
import 'logic/models/event/store_item.dart' as _i50;
import 'logic/models/json/event/booking.dart' as _i44;
import 'logic/models/json/user/portailUser.dart' as _i47;
import 'logic/models/role.dart' as _i52;
import 'pages/app_wrapper.dart' as _i1;
import 'pages/assos/asso_detail_page.dart' as _i33;
import 'pages/assos/asso_page.dart' as _i32;
import 'pages/assos/asso_wrapper_page.dart' as _i13;
import 'pages/bookings/bookingConfirmation.dart' as _i23;
import 'pages/bookings/bookingPage.dart' as _i22;
import 'pages/chats/room_list_page.dart' as _i36;
import 'pages/chats/room_list_wrapper.dart' as _i31;
import 'pages/event/edit/event_edit_booking_page.dart' as _i12;
import 'pages/event/edit/event_edit_description_page.dart' as _i11;
import 'pages/event/edit/event_edit_page.dart' as _i10;
import 'pages/event/event_booking_detail_page.dart' as _i7;
import 'pages/event/event_bookings_page.dart' as _i6;
import 'pages/event/event_create_page.dart' as _i8;
import 'pages/event/event_detail_page.dart' as _i14;
import 'pages/event/event_forms_page.dart' as _i9;
import 'pages/event/events_list_page.dart' as _i5;
import 'pages/feed_page.dart' as _i4;
import 'pages/home_page.dart' as _i3;
import 'pages/rentals/rental_create_page.dart' as _i26;
import 'pages/rentals/rental_detail_page.dart' as _i25;
import 'pages/rentals/rentals_page.dart' as _i24;
import 'pages/settings/settings_portail_profile_page.dart' as _i21;
import 'pages/settings/settings_profile_page.dart' as _i18;
import 'pages/settings/settings_security_page.dart' as _i19;
import 'pages/settings/settings_theme_page.dart' as _i20;
import 'pages/store/store_item_checkout_page.dart' as _i30;
import 'pages/store/store_item_creation.dart' as _i28;
import 'pages/store/store_item_page.dart' as _i29;
import 'pages/store/store_page.dart' as _i27;
import 'pages/user/roles/roles_detail_page.dart' as _i35;
import 'pages/user/roles/roles_list_page.dart' as _i34;
import 'pages/user/user_account.dart' as _i15;
import 'pages/user/user_detail_page.dart' as _i16;
import 'pages/user/users_list_page.dart' as _i17;
import 'pages/welcome/login/login_CAS.dart' as _i40;
import 'pages/welcome/login/loginMatrixPage.dart' as _i41;
import 'pages/welcome/welcome_page.dart' as _i39;

class AppRouter extends _i42.RootStackRouter {
  AppRouter([_i43.GlobalKey<_i43.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i42.PageFactory> pagesMap = {
    AppWrapperRoute.name: (routeData) {
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i1.AppWrapperPage(),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    WelcomeRouter.name: (routeData) {
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i2.EmptyRouterPage(),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    HomeRoute.name: (routeData) {
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i3.HomePage(),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    FeedRoute.name: (routeData) {
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i4.FeedPage(),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    EventsRoute.name: (routeData) {
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i5.EventsListPage(),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    EventBookingsRoute.name: (routeData) {
      final args = routeData.argsAs<EventBookingsRouteArgs>();
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i6.EventBookingsPage(args.bookings, key: args.key),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    EventBookingDetailRoute.name: (routeData) {
      final args = routeData.argsAs<EventBookingDetailRouteArgs>();
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i7.EventBookingDetailPage(args.booking, key: args.key),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    EventCreateRoute.name: (routeData) {
      final args = routeData.argsAs<EventCreateRouteArgs>();
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i8.EventCreatePage(key: args.key, event: args.event),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    EventFormsRoute.name: (routeData) {
      final args = routeData.argsAs<EventFormsRouteArgs>();
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i9.EventFormsPage(key: args.key, event: args.event),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    EventEditRoute.name: (routeData) {
      final args = routeData.argsAs<EventEditRouteArgs>();
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i10.EventEditPage(key: args.key, event: args.event),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    EventEditDescriptionRoute.name: (routeData) {
      final args = routeData.argsAs<EventEditDescriptionRouteArgs>();
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child:
              _i11.EventEditDescriptionPage(key: args.key, event: args.event),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    EventEditBookingRoute.name: (routeData) {
      final args = routeData.argsAs<EventEditBookingRouteArgs>();
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i12.EventEditBookingPage(key: args.key, event: args.event),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    AssoWrapperRoute.name: (routeData) {
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i13.AssoWrapperPage(),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    EventDetailRoute.name: (routeData) {
      final args = routeData.argsAs<EventDetailRouteArgs>();
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i14.EventDetailPage(args.event,
              key: args.key, isPreview: args.isPreview),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    UserRoute.name: (routeData) {
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i15.UserPage(),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    UserDetailRoute.name: (routeData) {
      final args = routeData.argsAs<UserDetailRouteArgs>();
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i16.UserDetailPage(args.user, key: args.key),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    UserListRoute.name: (routeData) {
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i17.UserListPage(),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    SettingsAccountRoute.name: (routeData) {
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i18.SettingsAccountPage(),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    SettingsSecurityRoute.name: (routeData) {
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i19.SettingsSecurityPage(),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    SettingsThemeRoute.name: (routeData) {
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i20.SettingsThemePage(),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    SettingsPortailProfileRoute.name: (routeData) {
      final args = routeData.argsAs<SettingsPortailProfileRouteArgs>();
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child:
              _i21.SettingsPortailProfilePage(key: args.key, room: args.room),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    BookingRoute.name: (routeData) {
      final args = routeData.argsAs<BookingRouteArgs>();
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i22.BookingPage(args.e,
              reservationId: args.reservationId, isPreview: args.isPreview),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    BookingConfirmationRoute.name: (routeData) {
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i23.BookingConfirmationPage(),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    RentalsRoute.name: (routeData) {
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i24.RentalsPage(),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    RentalDetailRoute.name: (routeData) {
      final args = routeData.argsAs<RentalDetailRouteArgs>();
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i25.RentalDetailPage(args.reservation, key: args.key),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    RentalCreateRoute.name: (routeData) {
      final args = routeData.argsAs<RentalCreateRouteArgs>();
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i26.RentalCreatePage(key: args.key, rental: args.rental),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    StoreRoute.name: (routeData) {
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i27.StorePage(),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    StoreItemCreateRoute.name: (routeData) {
      final args = routeData.argsAs<StoreItemCreateRouteArgs>();
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i28.StoreItemCreatePage(key: args.key, item: args.item),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    StoreItemRoute.name: (routeData) {
      final args = routeData.argsAs<StoreItemRouteArgs>();
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i29.StoreItemPage(key: args.key, item: args.item),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    StoreItemCheckoutRoute.name: (routeData) {
      final args = routeData.argsAs<StoreItemCheckoutRouteArgs>();
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i30.StoreItemCheckoutPage(key: args.key, item: args.item),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    RoomListWrapperRoute.name: (routeData) {
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i31.RoomListWrapper(),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    AssoPlaceholderRoute.name: (routeData) {
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i13.AssoPlaceholderPage(),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    AssociationRoute.name: (routeData) {
      final args = routeData.argsAs<AssociationRouteArgs>();
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i32.AssociationPage(args.assoData, key: args.key),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    AssoDetailRoute.name: (routeData) {
      final args = routeData.argsAs<AssoDetailRouteArgs>();
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i33.AssoDetailPage(args.assoData, key: args.key),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    RolesListRoute.name: (routeData) {
      final args = routeData.argsAs<RolesListRouteArgs>(
          orElse: () => const RolesListRouteArgs());
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i34.RolesListPage(key: args.key, asso: args.asso),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    RolesDetailRoute.name: (routeData) {
      final args = routeData.argsAs<RolesDetailRouteArgs>();
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i35.RolesDetailPage(key: args.key, r: args.r),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    RoomListRoute.name: (routeData) {
      final args = routeData.argsAs<RoomListRouteArgs>(
          orElse: () => const RoomListRouteArgs());
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i36.RoomListPage(key: args.key, isMobile: args.isMobile),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    RoomListSpaceRoute.name: (routeData) {
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i37.RoomListSpace(),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    RoomListRoomRoute.name: (routeData) {
      final args = routeData.argsAs<RoomListRoomRouteArgs>(
          orElse: () => const RoomListRoomRouteArgs());
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i38.RoomListRoom(
              key: args.key,
              displaySettingsOnDesktop: args.displaySettingsOnDesktop),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    WelcomeRoute.name: (routeData) {
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i39.WelcomePage(),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    LoginRoute.name: (routeData) {
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: _i40.LoginPage(),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    MatrixLoginRoute.name: (routeData) {
      return _i42.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i41.MatrixLoginPage(),
          transitionsBuilder: _i42.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    }
  };

  @override
  List<_i42.RouteConfig> get routes => [
        _i42.RouteConfig(AppWrapperRoute.name, path: '/', children: [
          _i42.RouteConfig('#redirect',
              path: '',
              parent: AppWrapperRoute.name,
              redirectTo: 'home',
              fullMatch: true),
          _i42.RouteConfig(HomeRoute.name,
              path: 'home', parent: AppWrapperRoute.name),
          _i42.RouteConfig(FeedRoute.name,
              path: 'feed', parent: AppWrapperRoute.name),
          _i42.RouteConfig(EventsRoute.name,
              path: 'events', parent: AppWrapperRoute.name),
          _i42.RouteConfig(EventBookingsRoute.name,
              path: 'bookings', parent: AppWrapperRoute.name),
          _i42.RouteConfig(EventBookingDetailRoute.name,
              path: 'bookings/id', parent: AppWrapperRoute.name),
          _i42.RouteConfig(EventCreateRoute.name,
              path: 'event/create', parent: AppWrapperRoute.name),
          _i42.RouteConfig(EventFormsRoute.name,
              path: 'event/edit/forms', parent: AppWrapperRoute.name),
          _i42.RouteConfig(EventEditRoute.name,
              path: 'event/edit', parent: AppWrapperRoute.name),
          _i42.RouteConfig(EventEditDescriptionRoute.name,
              path: 'edit/description', parent: AppWrapperRoute.name),
          _i42.RouteConfig(EventEditBookingRoute.name,
              path: 'edit/booking', parent: AppWrapperRoute.name),
          _i42.RouteConfig(AssoWrapperRoute.name,
              path: 'assos',
              parent: AppWrapperRoute.name,
              children: [
                _i42.RouteConfig(AssoPlaceholderRoute.name,
                    path: '', parent: AssoWrapperRoute.name),
                _i42.RouteConfig(AssociationRoute.name,
                    path: 'assos/details', parent: AssoWrapperRoute.name),
                _i42.RouteConfig(AssoDetailRoute.name,
                    path: 'assos/id', parent: AssoWrapperRoute.name),
                _i42.RouteConfig(RolesListRoute.name,
                    path: 'assos/roles', parent: AssoWrapperRoute.name),
                _i42.RouteConfig(RolesDetailRoute.name,
                    path: 'assos/roles/id', parent: AssoWrapperRoute.name)
              ]),
          _i42.RouteConfig(EventDetailRoute.name,
              path: 'event/detail', parent: AppWrapperRoute.name),
          _i42.RouteConfig(EventEditDescriptionRoute.name,
              path: 'edit/description', parent: AppWrapperRoute.name),
          _i42.RouteConfig(EventEditBookingRoute.name,
              path: 'edit/booking', parent: AppWrapperRoute.name),
          _i42.RouteConfig(UserRoute.name,
              path: 'user', parent: AppWrapperRoute.name),
          _i42.RouteConfig(UserDetailRoute.name,
              path: 'user/detail', parent: AppWrapperRoute.name),
          _i42.RouteConfig(UserListRoute.name,
              path: 'user/users', parent: AppWrapperRoute.name),
          _i42.RouteConfig(SettingsAccountRoute.name,
              path: 'user/settings/profile', parent: AppWrapperRoute.name),
          _i42.RouteConfig(SettingsSecurityRoute.name,
              path: 'user/settings/security', parent: AppWrapperRoute.name),
          _i42.RouteConfig(SettingsThemeRoute.name,
              path: 'user/settings/theme', parent: AppWrapperRoute.name),
          _i42.RouteConfig(SettingsPortailProfileRoute.name,
              path: 'user/settings/portail_profile',
              parent: AppWrapperRoute.name),
          _i42.RouteConfig(BookingRoute.name,
              path: 'book', parent: AppWrapperRoute.name),
          _i42.RouteConfig(BookingConfirmationRoute.name,
              path: 'book/confirmation', parent: AppWrapperRoute.name),
          _i42.RouteConfig(RentalsRoute.name,
              path: 'reservation', parent: AppWrapperRoute.name),
          _i42.RouteConfig(RentalDetailRoute.name,
              path: 'reservation/id', parent: AppWrapperRoute.name),
          _i42.RouteConfig(RentalCreateRoute.name,
              path: 'resevation/create', parent: AppWrapperRoute.name),
          _i42.RouteConfig(StoreRoute.name,
              path: 'store', parent: AppWrapperRoute.name),
          _i42.RouteConfig(StoreItemCreateRoute.name,
              path: 'store/item/create', parent: AppWrapperRoute.name),
          _i42.RouteConfig(StoreItemRoute.name,
              path: 'store/item/detail', parent: AppWrapperRoute.name),
          _i42.RouteConfig(StoreItemCheckoutRoute.name,
              path: 'store/item/checkout', parent: AppWrapperRoute.name),
          _i42.RouteConfig(RoomListWrapperRoute.name,
              path: 'rooms',
              parent: AppWrapperRoute.name,
              children: [
                _i42.RouteConfig(RoomListRoute.name,
                    path: '', parent: RoomListWrapperRoute.name),
                _i42.RouteConfig(RoomListSpaceRoute.name,
                    path: 'space', parent: RoomListWrapperRoute.name),
                _i42.RouteConfig(RoomListRoomRoute.name,
                    path: ':roomId', parent: RoomListWrapperRoute.name)
              ]),
          _i42.RouteConfig('*#redirect',
              path: '*',
              parent: AppWrapperRoute.name,
              redirectTo: '',
              fullMatch: true)
        ]),
        _i42.RouteConfig(WelcomeRouter.name, path: 'welcome', children: [
          _i42.RouteConfig(WelcomeRoute.name,
              path: '', parent: WelcomeRouter.name),
          _i42.RouteConfig(LoginRoute.name,
              path: 'login', parent: WelcomeRouter.name),
          _i42.RouteConfig(MatrixLoginRoute.name,
              path: 'login_matrix', parent: WelcomeRouter.name),
          _i42.RouteConfig('*#redirect',
              path: '*',
              parent: WelcomeRouter.name,
              redirectTo: '',
              fullMatch: true)
        ])
      ];
}

/// generated route for
/// [_i1.AppWrapperPage]
class AppWrapperRoute extends _i42.PageRouteInfo<void> {
  const AppWrapperRoute({List<_i42.PageRouteInfo>? children})
      : super(AppWrapperRoute.name, path: '/', initialChildren: children);

  static const String name = 'AppWrapperRoute';
}

/// generated route for
/// [_i2.EmptyRouterPage]
class WelcomeRouter extends _i42.PageRouteInfo<void> {
  const WelcomeRouter({List<_i42.PageRouteInfo>? children})
      : super(WelcomeRouter.name, path: 'welcome', initialChildren: children);

  static const String name = 'WelcomeRouter';
}

/// generated route for
/// [_i3.HomePage]
class HomeRoute extends _i42.PageRouteInfo<void> {
  const HomeRoute() : super(HomeRoute.name, path: 'home');

  static const String name = 'HomeRoute';
}

/// generated route for
/// [_i4.FeedPage]
class FeedRoute extends _i42.PageRouteInfo<void> {
  const FeedRoute() : super(FeedRoute.name, path: 'feed');

  static const String name = 'FeedRoute';
}

/// generated route for
/// [_i5.EventsListPage]
class EventsRoute extends _i42.PageRouteInfo<void> {
  const EventsRoute() : super(EventsRoute.name, path: 'events');

  static const String name = 'EventsRoute';
}

/// generated route for
/// [_i6.EventBookingsPage]
class EventBookingsRoute extends _i42.PageRouteInfo<EventBookingsRouteArgs> {
  EventBookingsRoute({required List<_i44.Booking> bookings, _i43.Key? key})
      : super(EventBookingsRoute.name,
            path: 'bookings',
            args: EventBookingsRouteArgs(bookings: bookings, key: key));

  static const String name = 'EventBookingsRoute';
}

class EventBookingsRouteArgs {
  const EventBookingsRouteArgs({required this.bookings, this.key});

  final List<_i44.Booking> bookings;

  final _i43.Key? key;

  @override
  String toString() {
    return 'EventBookingsRouteArgs{bookings: $bookings, key: $key}';
  }
}

/// generated route for
/// [_i7.EventBookingDetailPage]
class EventBookingDetailRoute
    extends _i42.PageRouteInfo<EventBookingDetailRouteArgs> {
  EventBookingDetailRoute({required _i44.Booking booking, _i43.Key? key})
      : super(EventBookingDetailRoute.name,
            path: 'bookings/id',
            args: EventBookingDetailRouteArgs(booking: booking, key: key));

  static const String name = 'EventBookingDetailRoute';
}

class EventBookingDetailRouteArgs {
  const EventBookingDetailRouteArgs({required this.booking, this.key});

  final _i44.Booking booking;

  final _i43.Key? key;

  @override
  String toString() {
    return 'EventBookingDetailRouteArgs{booking: $booking, key: $key}';
  }
}

/// generated route for
/// [_i8.EventCreatePage]
class EventCreateRoute extends _i42.PageRouteInfo<EventCreateRouteArgs> {
  EventCreateRoute({_i43.Key? key, required _i45.BasePortailEvent event})
      : super(EventCreateRoute.name,
            path: 'event/create',
            args: EventCreateRouteArgs(key: key, event: event));

  static const String name = 'EventCreateRoute';
}

class EventCreateRouteArgs {
  const EventCreateRouteArgs({this.key, required this.event});

  final _i43.Key? key;

  final _i45.BasePortailEvent event;

  @override
  String toString() {
    return 'EventCreateRouteArgs{key: $key, event: $event}';
  }
}

/// generated route for
/// [_i9.EventFormsPage]
class EventFormsRoute extends _i42.PageRouteInfo<EventFormsRouteArgs> {
  EventFormsRoute({_i43.Key? key, required _i46.PortailEvent event})
      : super(EventFormsRoute.name,
            path: 'event/edit/forms',
            args: EventFormsRouteArgs(key: key, event: event));

  static const String name = 'EventFormsRoute';
}

class EventFormsRouteArgs {
  const EventFormsRouteArgs({this.key, required this.event});

  final _i43.Key? key;

  final _i46.PortailEvent event;

  @override
  String toString() {
    return 'EventFormsRouteArgs{key: $key, event: $event}';
  }
}

/// generated route for
/// [_i10.EventEditPage]
class EventEditRoute extends _i42.PageRouteInfo<EventEditRouteArgs> {
  EventEditRoute({_i43.Key? key, required _i46.PortailEvent event})
      : super(EventEditRoute.name,
            path: 'event/edit',
            args: EventEditRouteArgs(key: key, event: event));

  static const String name = 'EventEditRoute';
}

class EventEditRouteArgs {
  const EventEditRouteArgs({this.key, required this.event});

  final _i43.Key? key;

  final _i46.PortailEvent event;

  @override
  String toString() {
    return 'EventEditRouteArgs{key: $key, event: $event}';
  }
}

/// generated route for
/// [_i11.EventEditDescriptionPage]
class EventEditDescriptionRoute
    extends _i42.PageRouteInfo<EventEditDescriptionRouteArgs> {
  EventEditDescriptionRoute({_i43.Key? key, required _i46.PortailEvent event})
      : super(EventEditDescriptionRoute.name,
            path: 'edit/description',
            args: EventEditDescriptionRouteArgs(key: key, event: event));

  static const String name = 'EventEditDescriptionRoute';
}

class EventEditDescriptionRouteArgs {
  const EventEditDescriptionRouteArgs({this.key, required this.event});

  final _i43.Key? key;

  final _i46.PortailEvent event;

  @override
  String toString() {
    return 'EventEditDescriptionRouteArgs{key: $key, event: $event}';
  }
}

/// generated route for
/// [_i12.EventEditBookingPage]
class EventEditBookingRoute
    extends _i42.PageRouteInfo<EventEditBookingRouteArgs> {
  EventEditBookingRoute({_i43.Key? key, required _i46.PortailEvent event})
      : super(EventEditBookingRoute.name,
            path: 'edit/booking',
            args: EventEditBookingRouteArgs(key: key, event: event));

  static const String name = 'EventEditBookingRoute';
}

class EventEditBookingRouteArgs {
  const EventEditBookingRouteArgs({this.key, required this.event});

  final _i43.Key? key;

  final _i46.PortailEvent event;

  @override
  String toString() {
    return 'EventEditBookingRouteArgs{key: $key, event: $event}';
  }
}

/// generated route for
/// [_i13.AssoWrapperPage]
class AssoWrapperRoute extends _i42.PageRouteInfo<void> {
  const AssoWrapperRoute({List<_i42.PageRouteInfo>? children})
      : super(AssoWrapperRoute.name, path: 'assos', initialChildren: children);

  static const String name = 'AssoWrapperRoute';
}

/// generated route for
/// [_i14.EventDetailPage]
class EventDetailRoute extends _i42.PageRouteInfo<EventDetailRouteArgs> {
  EventDetailRoute(
      {required _i46.PortailEvent event, _i43.Key? key, bool isPreview = false})
      : super(EventDetailRoute.name,
            path: 'event/detail',
            args: EventDetailRouteArgs(
                event: event, key: key, isPreview: isPreview));

  static const String name = 'EventDetailRoute';
}

class EventDetailRouteArgs {
  const EventDetailRouteArgs(
      {required this.event, this.key, this.isPreview = false});

  final _i46.PortailEvent event;

  final _i43.Key? key;

  final bool isPreview;

  @override
  String toString() {
    return 'EventDetailRouteArgs{event: $event, key: $key, isPreview: $isPreview}';
  }
}

/// generated route for
/// [_i15.UserPage]
class UserRoute extends _i42.PageRouteInfo<void> {
  const UserRoute() : super(UserRoute.name, path: 'user');

  static const String name = 'UserRoute';
}

/// generated route for
/// [_i16.UserDetailPage]
class UserDetailRoute extends _i42.PageRouteInfo<UserDetailRouteArgs> {
  UserDetailRoute({required _i47.PortailUser user, _i43.Key? key})
      : super(UserDetailRoute.name,
            path: 'user/detail',
            args: UserDetailRouteArgs(user: user, key: key));

  static const String name = 'UserDetailRoute';
}

class UserDetailRouteArgs {
  const UserDetailRouteArgs({required this.user, this.key});

  final _i47.PortailUser user;

  final _i43.Key? key;

  @override
  String toString() {
    return 'UserDetailRouteArgs{user: $user, key: $key}';
  }
}

/// generated route for
/// [_i17.UserListPage]
class UserListRoute extends _i42.PageRouteInfo<void> {
  const UserListRoute() : super(UserListRoute.name, path: 'user/users');

  static const String name = 'UserListRoute';
}

/// generated route for
/// [_i18.SettingsAccountPage]
class SettingsAccountRoute extends _i42.PageRouteInfo<void> {
  const SettingsAccountRoute()
      : super(SettingsAccountRoute.name, path: 'user/settings/profile');

  static const String name = 'SettingsAccountRoute';
}

/// generated route for
/// [_i19.SettingsSecurityPage]
class SettingsSecurityRoute extends _i42.PageRouteInfo<void> {
  const SettingsSecurityRoute()
      : super(SettingsSecurityRoute.name, path: 'user/settings/security');

  static const String name = 'SettingsSecurityRoute';
}

/// generated route for
/// [_i20.SettingsThemePage]
class SettingsThemeRoute extends _i42.PageRouteInfo<void> {
  const SettingsThemeRoute()
      : super(SettingsThemeRoute.name, path: 'user/settings/theme');

  static const String name = 'SettingsThemeRoute';
}

/// generated route for
/// [_i21.SettingsPortailProfilePage]
class SettingsPortailProfileRoute
    extends _i42.PageRouteInfo<SettingsPortailProfileRouteArgs> {
  SettingsPortailProfileRoute({_i43.Key? key, required _i48.Room room})
      : super(SettingsPortailProfileRoute.name,
            path: 'user/settings/portail_profile',
            args: SettingsPortailProfileRouteArgs(key: key, room: room));

  static const String name = 'SettingsPortailProfileRoute';
}

class SettingsPortailProfileRouteArgs {
  const SettingsPortailProfileRouteArgs({this.key, required this.room});

  final _i43.Key? key;

  final _i48.Room room;

  @override
  String toString() {
    return 'SettingsPortailProfileRouteArgs{key: $key, room: $room}';
  }
}

/// generated route for
/// [_i22.BookingPage]
class BookingRoute extends _i42.PageRouteInfo<BookingRouteArgs> {
  BookingRoute(
      {required _i46.PortailEvent? e,
      int? reservationId,
      bool isPreview = false})
      : super(BookingRoute.name,
            path: 'book',
            args: BookingRouteArgs(
                e: e, reservationId: reservationId, isPreview: isPreview));

  static const String name = 'BookingRoute';
}

class BookingRouteArgs {
  const BookingRouteArgs(
      {required this.e, this.reservationId, this.isPreview = false});

  final _i46.PortailEvent? e;

  final int? reservationId;

  final bool isPreview;

  @override
  String toString() {
    return 'BookingRouteArgs{e: $e, reservationId: $reservationId, isPreview: $isPreview}';
  }
}

/// generated route for
/// [_i23.BookingConfirmationPage]
class BookingConfirmationRoute extends _i42.PageRouteInfo<void> {
  const BookingConfirmationRoute()
      : super(BookingConfirmationRoute.name, path: 'book/confirmation');

  static const String name = 'BookingConfirmationRoute';
}

/// generated route for
/// [_i24.RentalsPage]
class RentalsRoute extends _i42.PageRouteInfo<void> {
  const RentalsRoute() : super(RentalsRoute.name, path: 'reservation');

  static const String name = 'RentalsRoute';
}

/// generated route for
/// [_i25.RentalDetailPage]
class RentalDetailRoute extends _i42.PageRouteInfo<RentalDetailRouteArgs> {
  RentalDetailRoute({required _i49.RentalItem reservation, _i43.Key? key})
      : super(RentalDetailRoute.name,
            path: 'reservation/id',
            args: RentalDetailRouteArgs(reservation: reservation, key: key));

  static const String name = 'RentalDetailRoute';
}

class RentalDetailRouteArgs {
  const RentalDetailRouteArgs({required this.reservation, this.key});

  final _i49.RentalItem reservation;

  final _i43.Key? key;

  @override
  String toString() {
    return 'RentalDetailRouteArgs{reservation: $reservation, key: $key}';
  }
}

/// generated route for
/// [_i26.RentalCreatePage]
class RentalCreateRoute extends _i42.PageRouteInfo<RentalCreateRouteArgs> {
  RentalCreateRoute({_i43.Key? key, required _i49.RentalItem rental})
      : super(RentalCreateRoute.name,
            path: 'resevation/create',
            args: RentalCreateRouteArgs(key: key, rental: rental));

  static const String name = 'RentalCreateRoute';
}

class RentalCreateRouteArgs {
  const RentalCreateRouteArgs({this.key, required this.rental});

  final _i43.Key? key;

  final _i49.RentalItem rental;

  @override
  String toString() {
    return 'RentalCreateRouteArgs{key: $key, rental: $rental}';
  }
}

/// generated route for
/// [_i27.StorePage]
class StoreRoute extends _i42.PageRouteInfo<void> {
  const StoreRoute() : super(StoreRoute.name, path: 'store');

  static const String name = 'StoreRoute';
}

/// generated route for
/// [_i28.StoreItemCreatePage]
class StoreItemCreateRoute
    extends _i42.PageRouteInfo<StoreItemCreateRouteArgs> {
  StoreItemCreateRoute({_i43.Key? key, required _i50.StoreItem item})
      : super(StoreItemCreateRoute.name,
            path: 'store/item/create',
            args: StoreItemCreateRouteArgs(key: key, item: item));

  static const String name = 'StoreItemCreateRoute';
}

class StoreItemCreateRouteArgs {
  const StoreItemCreateRouteArgs({this.key, required this.item});

  final _i43.Key? key;

  final _i50.StoreItem item;

  @override
  String toString() {
    return 'StoreItemCreateRouteArgs{key: $key, item: $item}';
  }
}

/// generated route for
/// [_i29.StoreItemPage]
class StoreItemRoute extends _i42.PageRouteInfo<StoreItemRouteArgs> {
  StoreItemRoute({_i43.Key? key, required _i50.StoreItem item})
      : super(StoreItemRoute.name,
            path: 'store/item/detail',
            args: StoreItemRouteArgs(key: key, item: item));

  static const String name = 'StoreItemRoute';
}

class StoreItemRouteArgs {
  const StoreItemRouteArgs({this.key, required this.item});

  final _i43.Key? key;

  final _i50.StoreItem item;

  @override
  String toString() {
    return 'StoreItemRouteArgs{key: $key, item: $item}';
  }
}

/// generated route for
/// [_i30.StoreItemCheckoutPage]
class StoreItemCheckoutRoute
    extends _i42.PageRouteInfo<StoreItemCheckoutRouteArgs> {
  StoreItemCheckoutRoute({_i43.Key? key, required _i50.StoreItem item})
      : super(StoreItemCheckoutRoute.name,
            path: 'store/item/checkout',
            args: StoreItemCheckoutRouteArgs(key: key, item: item));

  static const String name = 'StoreItemCheckoutRoute';
}

class StoreItemCheckoutRouteArgs {
  const StoreItemCheckoutRouteArgs({this.key, required this.item});

  final _i43.Key? key;

  final _i50.StoreItem item;

  @override
  String toString() {
    return 'StoreItemCheckoutRouteArgs{key: $key, item: $item}';
  }
}

/// generated route for
/// [_i31.RoomListWrapper]
class RoomListWrapperRoute extends _i42.PageRouteInfo<void> {
  const RoomListWrapperRoute({List<_i42.PageRouteInfo>? children})
      : super(RoomListWrapperRoute.name,
            path: 'rooms', initialChildren: children);

  static const String name = 'RoomListWrapperRoute';
}

/// generated route for
/// [_i13.AssoPlaceholderPage]
class AssoPlaceholderRoute extends _i42.PageRouteInfo<void> {
  const AssoPlaceholderRoute() : super(AssoPlaceholderRoute.name, path: '');

  static const String name = 'AssoPlaceholderRoute';
}

/// generated route for
/// [_i32.AssociationPage]
class AssociationRoute extends _i42.PageRouteInfo<AssociationRouteArgs> {
  AssociationRoute({required _i51.AssociationData assoData, _i43.Key? key})
      : super(AssociationRoute.name,
            path: 'assos/details',
            args: AssociationRouteArgs(assoData: assoData, key: key));

  static const String name = 'AssociationRoute';
}

class AssociationRouteArgs {
  const AssociationRouteArgs({required this.assoData, this.key});

  final _i51.AssociationData assoData;

  final _i43.Key? key;

  @override
  String toString() {
    return 'AssociationRouteArgs{assoData: $assoData, key: $key}';
  }
}

/// generated route for
/// [_i33.AssoDetailPage]
class AssoDetailRoute extends _i42.PageRouteInfo<AssoDetailRouteArgs> {
  AssoDetailRoute({required _i51.AssociationData? assoData, _i43.Key? key})
      : super(AssoDetailRoute.name,
            path: 'assos/id',
            args: AssoDetailRouteArgs(assoData: assoData, key: key));

  static const String name = 'AssoDetailRoute';
}

class AssoDetailRouteArgs {
  const AssoDetailRouteArgs({required this.assoData, this.key});

  final _i51.AssociationData? assoData;

  final _i43.Key? key;

  @override
  String toString() {
    return 'AssoDetailRouteArgs{assoData: $assoData, key: $key}';
  }
}

/// generated route for
/// [_i34.RolesListPage]
class RolesListRoute extends _i42.PageRouteInfo<RolesListRouteArgs> {
  RolesListRoute({_i43.Key? key, _i51.AssociationData? asso})
      : super(RolesListRoute.name,
            path: 'assos/roles',
            args: RolesListRouteArgs(key: key, asso: asso));

  static const String name = 'RolesListRoute';
}

class RolesListRouteArgs {
  const RolesListRouteArgs({this.key, this.asso});

  final _i43.Key? key;

  final _i51.AssociationData? asso;

  @override
  String toString() {
    return 'RolesListRouteArgs{key: $key, asso: $asso}';
  }
}

/// generated route for
/// [_i35.RolesDetailPage]
class RolesDetailRoute extends _i42.PageRouteInfo<RolesDetailRouteArgs> {
  RolesDetailRoute({_i43.Key? key, required _i52.Role r})
      : super(RolesDetailRoute.name,
            path: 'assos/roles/id', args: RolesDetailRouteArgs(key: key, r: r));

  static const String name = 'RolesDetailRoute';
}

class RolesDetailRouteArgs {
  const RolesDetailRouteArgs({this.key, required this.r});

  final _i43.Key? key;

  final _i52.Role r;

  @override
  String toString() {
    return 'RolesDetailRouteArgs{key: $key, r: $r}';
  }
}

/// generated route for
/// [_i36.RoomListPage]
class RoomListRoute extends _i42.PageRouteInfo<RoomListRouteArgs> {
  RoomListRoute({_i43.Key? key, bool isMobile = false})
      : super(RoomListRoute.name,
            path: '', args: RoomListRouteArgs(key: key, isMobile: isMobile));

  static const String name = 'RoomListRoute';
}

class RoomListRouteArgs {
  const RoomListRouteArgs({this.key, this.isMobile = false});

  final _i43.Key? key;

  final bool isMobile;

  @override
  String toString() {
    return 'RoomListRouteArgs{key: $key, isMobile: $isMobile}';
  }
}

/// generated route for
/// [_i37.RoomListSpace]
class RoomListSpaceRoute extends _i42.PageRouteInfo<void> {
  const RoomListSpaceRoute() : super(RoomListSpaceRoute.name, path: 'space');

  static const String name = 'RoomListSpaceRoute';
}

/// generated route for
/// [_i38.RoomListRoom]
class RoomListRoomRoute extends _i42.PageRouteInfo<RoomListRoomRouteArgs> {
  RoomListRoomRoute({_i43.Key? key, bool displaySettingsOnDesktop = false})
      : super(RoomListRoomRoute.name,
            path: ':roomId',
            args: RoomListRoomRouteArgs(
                key: key, displaySettingsOnDesktop: displaySettingsOnDesktop));

  static const String name = 'RoomListRoomRoute';
}

class RoomListRoomRouteArgs {
  const RoomListRoomRouteArgs(
      {this.key, this.displaySettingsOnDesktop = false});

  final _i43.Key? key;

  final bool displaySettingsOnDesktop;

  @override
  String toString() {
    return 'RoomListRoomRouteArgs{key: $key, displaySettingsOnDesktop: $displaySettingsOnDesktop}';
  }
}

/// generated route for
/// [_i39.WelcomePage]
class WelcomeRoute extends _i42.PageRouteInfo<void> {
  const WelcomeRoute() : super(WelcomeRoute.name, path: '');

  static const String name = 'WelcomeRoute';
}

/// generated route for
/// [_i40.LoginPage]
class LoginRoute extends _i42.PageRouteInfo<void> {
  const LoginRoute() : super(LoginRoute.name, path: 'login');

  static const String name = 'LoginRoute';
}

/// generated route for
/// [_i41.MatrixLoginPage]
class MatrixLoginRoute extends _i42.PageRouteInfo<void> {
  const MatrixLoginRoute() : super(MatrixLoginRoute.name, path: 'login_matrix');

  static const String name = 'MatrixLoginRoute';
}
