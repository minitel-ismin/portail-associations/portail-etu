import 'package:auto_route/auto_route.dart';

import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:emse_bde_app/logic/models/event/store_item.dart';
import 'package:emse_bde_app/partials/controls/custom_list.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/material.dart';
import 'package:minestrix_chat/partials/matrix/matrix_image_avatar.dart';

class StoreItemPage extends StatefulWidget {
  const StoreItemPage({Key? key, required this.item}) : super(key: key);
  final StoreItem item;

  @override
  _StoreItemPageState createState() => _StoreItemPageState();
}

class _StoreItemPageState extends State<StoreItemPage> {
  @override
  Widget build(BuildContext context) {
    final m = Matrix.of(context).client;

    return CustomList(
      title: widget.item.name,
      children: [
        Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: MatrixImageAvatar(
                url: widget.item.r.avatar,
                defaultText: widget.item.name,
                backgroundColor: Theme.of(context).primaryColor,
                width: 400,
                height: 400,
                client: m),
          ),
        ),
        Column(
          children: [
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 40, horizontal: 8.0),
              child: Text(widget.item.description),
            ),
            Card(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Text(widget.item.eventBooking.displayCost,
                    style:
                        TextStyle(fontSize: 17, fontWeight: FontWeight.w600)),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: ElevatedButton(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(Icons.edit),
                        SizedBox(width: 12),
                        Text("Edit"),
                      ],
                    ),
                  ),
                  style: ElevatedButton.styleFrom(primary: Colors.blue),
                  onPressed: () async {
                    await context
                        .navigateTo(StoreItemCreateRoute(item: widget.item));
                    setState(() {});
                  }),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: ElevatedButton(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(Icons.arrow_forward),
                        SizedBox(width: 12),
                        Text("Acheter"),
                      ],
                    ),
                  ),
                  style: ElevatedButton.styleFrom(primary: Colors.green),
                  onPressed: () async {
                    await context
                        .navigateTo(StoreItemCheckoutRoute(item: widget.item));
                    setState(() {});
                  }),
            ),
          ],
        ),
      ],
    );
  }
}
