import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:emse_bde_app/logic/models/event/portail_event.dart';
import 'package:emse_bde_app/logic/models/json/event/portail_event_booking.dart';
import 'package:emse_bde_app/partials/controls/custom_list.dart';
import 'package:emse_bde_app/partials/events/event_participant_card.dart';
import 'package:emse_bde_app/partials/helpers/preview_indicator.dart';
import 'package:emse_bde_app/partials/helpers/text.dart';
import 'package:emse_bde_app/partials/icon_text.dart';
import 'package:emse_bde_app/partials/matrix/chat_button.dart';
import 'package:emse_bde_app/partials/news/feed_widget.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/minestrix_chat.dart';
import 'package:minestrix_chat/partials/calendar_event/calendar_event_widget.dart';
import 'package:minestrix_chat/partials/chat/room/room_participants_indicator.dart';
import 'package:minestrix_chat/partials/dialogs/custom_dialogs.dart';
import 'package:minestrix_chat/partials/matrix/matrix_image_avatar.dart';
import 'package:minestrix_chat/utils/extensions/minestrix/model/calendar_event_model.dart';
import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:minestrix_chat/utils/poll/poll.dart';
import 'package:minestrix_chat/view/room_page.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../partials/events/asso_creator_display.dart';
import '../../partials/helpers/date_selector.dart';
import '../../utils/date_time_extension.dart';

class EventDetailPage extends StatefulWidget {
  const EventDetailPage(this.event, {Key? key, this.isPreview = false})
      : super(key: key);

  final PortailEvent event;
  final bool isPreview;

  @override
  _EventDetailPageState createState() => _EventDetailPageState();
}

class _EventDetailPageState extends State<EventDetailPage> {
  /// post load the room
  Future<bool> postLoadRoom() async {
    await event?.r.postLoad();
    return true;
  }

  String getStringTime(DateTime? date) {
    if (date == null) return "";
    return date.day.toString().padLeft(2, "0") +
        "/" +
        date.month.toString().padLeft(2, "0") +
        "/" +
        date.year.toString() +
        " à " +
        date.hour.toString() +
        "h" +
        date.minute.toString().padLeft(2, "0");
  }

  Widget getRemainingTime(DateTime date,
      {bool closeDate: false, bool shotgunDate: false}) {
    int remainingTime = ((date.toLocal().millisecondsSinceEpoch -
                DateTime.now().millisecondsSinceEpoch) /
            1000)
        .floor();
    if (remainingTime < 0) {
      if (shotgunDate) return Text("Le shotgun a commencé");
      if (closeDate)
        return Text("Les inscrptions sont closes",
            style: TextStyle(fontWeight: FontWeight.w600));
    }
    String text = "";
    if (closeDate) text = "Fin des inscriptions est dans ";
    if (shotgunDate) text = "Shotgun dans ";

    text += (remainingTime / (3600 * 24)).floor().toString() +
        " jours " +
        ((remainingTime % 3600 * 24) / 3600).floor().toString() +
        " heures " +
        ((remainingTime % 3600) / 60).floor().toString() +
        " minutes " +
        (remainingTime % (60)).toString() +
        " secondes";

    text += " (" + getStringTime(date) + ")";
    return Text(text);
  }

  PortailEvent? event;
  @override
  void initState() {
    super.initState();
    event = widget.event;
  }

  @override
  Widget build(BuildContext context) {
    final m = Matrix.of(context).client;

    return Scaffold(
      body: Builder(builder: (context) {
        if (this.event == null) return Text("No event");

        final event = this.event!;
        final room = event.r;

        return FutureBuilder(
            future: postLoadRoom(),
            builder: (context, snap) {
              return Row(
                children: [
                  Expanded(
                    child: StreamBuilder<Object>(
                        stream: m.onSync.stream,
                        builder: (context, snapshot) {
                          return Builder(builder: (context) {
                            final calendarEvent =
                                room.getEventAttendanceEvent();
                            return CustomList(
                              title: event.name,
                              actions: [
                                if (event.r.canSendDefaultStates == true)
                                  IconButton(
                                      icon: Icon(Icons.info),
                                      onPressed: () async {
                                        await context.pushRoute(
                                            EventEditRoute(event: event));
                                        if (mounted) setState(() {});
                                      })
                              ],
                              hasData: snap.hasData,
                              children: [
                                MatrixImageAvatar(
                                  client: Matrix.of(context).client,
                                  url: event.avatar,
                                  width: 2000,
                                  shape: MatrixImageAvatarShape.none,
                                  height: 250,
                                  defaultText: event.name,
                                  thumnailOnly:
                                      false, // we don't use thumnail as this picture is from weird dimmension and preview generation don't work well
                                  backgroundColor: Colors.blue,
                                ),
                                if (widget.isPreview) PreviewIndicator(),
                                SizedBox(height: 6),
                                Wrap(
                                  alignment: WrapAlignment.center,
                                  children: [
                                    ConstrainedBox(
                                      constraints:
                                          BoxConstraints(maxWidth: 600),
                                      child: Padding(
                                        padding: const EdgeInsets.all(20.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Card(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            14.0),
                                                    child: Text(event.name,
                                                        style: TextStyle(
                                                            fontSize: 24,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600)),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            8.0),
                                                    child: Row(
                                                      children: [
                                                        Expanded(
                                                          child:
                                                              RoomParticipantsIndicator(
                                                                  room:
                                                                      event.r),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  FutureBuilder<Event?>(
                                                      future: event
                                                          .getEventAttendanceEvent(),
                                                      builder:
                                                          (context, snapshot) {
                                                        if (snapshot.hasData ==
                                                            false)
                                                          return Container();

                                                        Poll p = Poll(
                                                            e: snapshot.data!,
                                                            t: event.t!);
                                                        return Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  vertical:
                                                                      12.0),
                                                          child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                              children: [
                                                                Card(
                                                                    color: Theme.of(
                                                                            context)
                                                                        .primaryColor,
                                                                    child: CalendarEventWidget(
                                                                        p: p)),
                                                              ]),
                                                        );
                                                      }),
                                                  if (event.description != "")
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              20),
                                                      child: MarkdownBody(
                                                          data:
                                                              event.description,
                                                          onTapLink: (text,
                                                              href,
                                                              title) async {
                                                            if (href != null) {
                                                              if (await canLaunchUrl(
                                                                  Uri.parse(
                                                                      href))) {
                                                                await launchUrl(
                                                                    Uri.parse(
                                                                        href));
                                                              } else {
                                                                throw 'Could not launch $href';
                                                              }
                                                            }
                                                          }),
                                                    ),
                                                  AssoCreatorDisplay(
                                                      room: event.r),
                                                  Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 12.0),
                                                    child: Text("Informations",
                                                        style: TextStyle(
                                                            fontSize: 22,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500)),
                                                  ),
                                                  if (room.canSendDefaultStates ||
                                                      calendarEvent?.place !=
                                                          null)
                                                    Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        const Text("Lieux"),
                                                        ListTile(
                                                            title: Text(calendarEvent
                                                                        ?.place !=
                                                                    null
                                                                ? calendarEvent!
                                                                    .place!
                                                                : "Set place"),
                                                            leading: const Icon(
                                                                Icons
                                                                    .location_on),
                                                            onTap: !room
                                                                    .canSendDefaultStates
                                                                ? null
                                                                : () async {
                                                                    String?
                                                                        place =
                                                                        await CustomDialogs
                                                                            .showCustomTextDialog(
                                                                      context,
                                                                      title:
                                                                          "Set the event location",
                                                                      helpText:
                                                                          "Event location",
                                                                      initialText:
                                                                          calendarEvent?.place ??
                                                                              '',
                                                                    );
                                                                    if (place !=
                                                                        null) {
                                                                      calendarEvent
                                                                              ?.place =
                                                                          place;
                                                                      if (calendarEvent !=
                                                                          null)
                                                                        await room
                                                                            .sendCalendarEventState(calendarEvent);
                                                                    }
                                                                  }),
                                                      ],
                                                    ),
                                                  if (event.eventDescription
                                                          .location !=
                                                      null)
                                                    ListTile(
                                                        leading:
                                                            Icon(Icons.map),
                                                        title: Text("Lieu"),
                                                        subtitle: Text(event
                                                            .eventDescription
                                                            .location!)),
                                                  if (calendarEvent != null)
                                                    Column(
                                                      children: [
                                                        ListTile(
                                                            title: const Text(
                                                                "Start"),
                                                            subtitle: Text(calendarEvent
                                                                    .start
                                                                    ?.toFormatedString() ??
                                                                "No start date"),
                                                            leading: const Icon(
                                                              Icons
                                                                  .calendar_today,
                                                            ),
                                                            onTap: !room
                                                                    .canSendDefaultStates
                                                                ? null
                                                                : () {
                                                                    DatePicker.showDateTimePicker(
                                                                        context,
                                                                        showTitleActions:
                                                                            true,
                                                                        currentTime:
                                                                            calendarEvent.start,
                                                                        onConfirm:
                                                                            (date) {
                                                                      calendarEvent
                                                                              .start =
                                                                          date;
                                                                      room.sendCalendarEventState(
                                                                          calendarEvent);
                                                                    });
                                                                  }),
                                                        DateSelector(
                                                            name:
                                                                "Début de l'évènement",
                                                            date: calendarEvent
                                                                .start,
                                                            onNewDate:
                                                                (date) async {
                                                              calendarEvent
                                                                  .start = date;
                                                              room.sendCalendarEventState(
                                                                  calendarEvent);
                                                            }),
                                                        DateSelector(
                                                            name:
                                                                "Fin de l'évènement",
                                                            date: calendarEvent
                                                                .end,
                                                            onNewDate:
                                                                (date) async {
                                                              calendarEvent
                                                                  .end = date;
                                                              room.sendCalendarEventState(
                                                                  calendarEvent);
                                                            }),
                                                        ListTile(
                                                            title: const Text(
                                                                "End"),
                                                            subtitle: Text(
                                                                calendarEvent
                                                                        .end
                                                                        ?.toFormatedString() ??
                                                                    "No end date"),
                                                            leading: const Icon(
                                                              Icons
                                                                  .calendar_today,
                                                            ),
                                                            onTap: !room
                                                                    .canSendDefaultStates
                                                                ? null
                                                                : () {
                                                                    DatePicker.showDateTimePicker(
                                                                        context,
                                                                        showTitleActions:
                                                                            true,
                                                                        currentTime:
                                                                            calendarEvent.end,
                                                                        onConfirm:
                                                                            (date) {
                                                                      calendarEvent
                                                                              .end =
                                                                          date;
                                                                      room.sendCalendarEventState(
                                                                          calendarEvent);
                                                                    });
                                                                  }),
                                                        if (calendarEvent
                                                                    .start !=
                                                                null &&
                                                            calendarEvent.end !=
                                                                null)
                                                          DurationWidget(
                                                              calendarEvent:
                                                                  calendarEvent),
                                                      ],
                                                    ),
                                                  ListTile(
                                                      leading: Icon(Icons.euro),
                                                      title: Text("Prix"),
                                                      subtitle: Text(event
                                                                  .eventBooking
                                                                  .cost !=
                                                              0.0
                                                          ? event.eventBooking
                                                              .displayCost
                                                          : "Gratuit")),
                                                  if (event.isBookable)
                                                    EventParticipantsCard(
                                                        event: event),
                                                ],
                                              ),
                                            ),
                                            SizedBox(height: 6),
                                            if (event.r.lastEvent != null)
                                              ChatButton(r: event.r),
                                            if (event.eventBooking.paymentMeans
                                                    .isNotEmpty ==
                                                true)
                                              Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        vertical: 8.0),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text("Moyen de paiement",
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold)),
                                                    for (PayementMeans p
                                                        in event.eventBooking
                                                            .paymentMeans)
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                left: 8.0),
                                                        child: Text(p.name),
                                                      ),
                                                  ],
                                                ),
                                              ),
                                            if ((event.eventBooking
                                                        .shotgunListLength ??
                                                    0) !=
                                                0)
                                              IconText(
                                                  title: "Event au shotgun",
                                                  text: (event.eventBooking
                                                                  .shotgunListLength! -
                                                              event.bookings
                                                                  .length)
                                                          .toString() +
                                                      " places disponibles",
                                                  subtitle: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Text("Le " +
                                                          getStringTime(event
                                                              .eventBooking
                                                              .bookingTimeStart)),
                                                      if (event.duration != "")
                                                        Text(event.duration
                                                            .toString()),
                                                    ],
                                                  ),
                                                  icon: Icons.people),
                                            if (event.isBookable &&
                                                (event.eventBooking
                                                            .hasShotgun ==
                                                        false ||
                                                    DateTime.now()
                                                            .millisecondsSinceEpoch >
                                                        event
                                                            .eventBooking
                                                            .bookingTimeStart!
                                                            .millisecondsSinceEpoch) &&
                                                (event.eventBooking
                                                            .bookingTimeEnd ==
                                                        null ||
                                                    event
                                                            .eventBooking
                                                            .bookingTimeEnd!
                                                            .millisecondsSinceEpoch >
                                                        DateTime.now()
                                                            .millisecondsSinceEpoch))
                                              Padding(
                                                padding:
                                                    const EdgeInsets.all(12.0),
                                                child: ElevatedButton(
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              10.0),
                                                      child: Row(
                                                        mainAxisSize:
                                                            MainAxisSize.min,
                                                        children: [
                                                          (snap.hasData ||
                                                                  widget
                                                                      .isPreview)
                                                              ? Icon(Icons
                                                                  .arrow_forward)
                                                              : SizedBox(
                                                                  width: 18,
                                                                  height: 18,
                                                                  child: CircularProgressIndicator(
                                                                      color: Colors
                                                                          .white)),
                                                          SizedBox(width: 12),
                                                          Text(
                                                              "Réservez moi !!"),
                                                        ],
                                                      ),
                                                    ),
                                                    style: ElevatedButton
                                                        .styleFrom(
                                                            primary:
                                                                Colors.green),
                                                    onPressed: () async {
                                                      if (snap.hasData ||
                                                          widget.isPreview) {
                                                        await context.navigateTo(
                                                            BookingRoute(
                                                                e: event,
                                                                isPreview: widget
                                                                    .isPreview));
                                                        setState(() {});
                                                      }
                                                    }),
                                              ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  if (event.eventBooking
                                                          .bookingTimeEnd !=
                                                      null)
                                                    getRemainingTime(
                                                        event.eventBooking
                                                            .bookingTimeEnd!,
                                                        shotgunDate: true),
                                                  if (event.eventDescription
                                                          .timeEnd !=
                                                      null)
                                                    getRemainingTime(
                                                        event.eventDescription
                                                            .timeEnd!,
                                                        closeDate: true),
                                                ],
                                              ),
                                            ),
                                            ListTile(
                                                leading: Icon(Icons.poll),
                                                title: Text("Sondages"),
                                                subtitle: Text(
                                                    "Afficher les sondages de l'évènement"),
                                                trailing:
                                                    Icon(Icons.navigate_next),
                                                onTap: () {
                                                  context.pushRoute(
                                                      EventFormsRoute(
                                                          event: event));
                                                })
                                          ],
                                        ),
                                      ),
                                    ),
                                    ConstrainedBox(
                                      constraints:
                                          BoxConstraints(maxWidth: 600),
                                      child: MatrixPostsWidget(
                                        room: event.r,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            );
                          });
                        }),
                  ),
                  SizedBox(
                      width: 400,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Card(
                          color: Theme.of(context).primaryColor,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Text(
                                  "Chat",
                                  style: TextStyle(
                                      fontSize: 28,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  color: Theme.of(context).cardColor,
                                  child:
                                      RoomPage(roomId: event.roomId, client: m),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ))
                ],
              );
            });
      }),
    );
  }
}

class DurationWidget extends StatelessWidget {
  const DurationWidget({
    Key? key,
    required this.calendarEvent,
  }) : super(key: key);

  final CalendarEvent calendarEvent;

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      final diff = calendarEvent.end!.difference(calendarEvent.start!);
      String text = "No duration";
      if (diff.inHours != 0) {
        text = "${diff.inHours} hour(s)";
      }
      if (diff.inMinutes.abs() > 0) {
        if (diff.inMinutes >= 60) {
          text += " and ${diff.inMinutes % 60} minute(s)";
        } else {
          text = "${diff.inMinutes} minute(s)";
        }
      }

      return ListTile(
          title: const Text("Duration"),
          subtitle: Text(text),
          leading: const Icon(Icons.date_range));
    });
  }
}
