import 'package:emse_bde_app/logic/models/event/portail_event.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/partials/events/event_edit_booking.dart';
import 'package:flutter/material.dart';

class EventEditBookingPage extends StatelessWidget {
  final PortailEvent event;
  EventEditBookingPage({Key? key, required this.event}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CustomHeader("Modifier l'évènement"),
        Expanded(
          child: EventEditBooking(
              event: event,
              onSave: () {
                Navigator.of(context).pop();
              }),
        ),
      ],
    );
  }
}
