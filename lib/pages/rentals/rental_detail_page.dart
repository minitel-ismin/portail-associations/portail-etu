import 'package:another_flushbar/flushbar.dart';
import 'package:auto_route/src/router/auto_router_x.dart';

import 'package:emse_bde_app/logic/models/event/rental_item.dart';
import 'package:emse_bde_app/partials/controls/custom_elevetated_button.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/partials/helpers/text.dart';
import 'package:emse_bde_app/partials/loading_bar.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/material.dart';
import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:timetable/timetable.dart';
import 'package:timetable/timetable.dart' as t;

class RentalDetailPage extends StatefulWidget {
  const RentalDetailPage(this.reservation, {Key? key}) : super(key: key);

  final RentalItem reservation;

  @override
  _RentalDetailPageState createState() => _RentalDetailPageState();
}

class _RentalDetailPageState extends State<RentalDetailPage> {
  late RentalItem res;

  @override
  void initState() {
    super.initState();
    res = widget.reservation;
  }

  List<RentalItemReservation> bookings = [];

  bool isBooking = false;

  Future<List<RentalItemReservation>> getBookings(bool forceRefresh) async {
    bookings = await widget.reservation.getReservations();
    return bookings;
  }

  final _dateController = DateController(
    // All parameters are optional and displayed with their default value.
    initialDate: DateTimeTimetable.today(),
    visibleRange: VisibleDateRange.week(startOfWeek: DateTime.monday),
  );

  final _timeController = TimeController(
    // All parameters are optional. By default, the whole day is revealed
    // initially and you can zoom in to view just a single minute.
    minDuration: Duration(minutes: 15), // The closest you can zoom in.
    maxDuration: Duration(hours: 23), // The farthest you can zoom out.
    initialRange: TimeRange(Duration(hours: 9), Duration(hours: 17)),
    maxRange: TimeRange(Duration(hours: 0), Duration(hours: 23)),
  );

  late BuildContext
      homeContext; // for bottom modal. Context is affected by set state
  @override
  Widget build(BuildContext context) {
    final m = Matrix.of(context).client;
    homeContext = context;

    return StreamBuilder<Object>(
        stream: m.onSync.stream,
        builder: (context, snapshot) {
          return Column(
            children: [
              CustomHeader("Réservation " + res.name, actionButton: [
                if (res.canSetEventBooking && res.canSetEventDescription)
                  IconButton(
                      icon: Icon(Icons.edit),
                      onPressed: () async {
                        await context.pushRoute(RentalCreateRoute(rental: res));
                        setState(() {});
                      }),
                IconButton(
                    icon: Icon(Icons.refresh),
                    onPressed: () async {
                      setState(() {
                        isBooking = true;
                      });
                      await getBookings(true);
                      setState(() {
                        isBooking = false;
                      });
                    })
              ]),
              isBooking
                  ? Center(child: LoadingBar("Chargement des réservation"))
                  : FutureBuilder<List<RentalItemReservation>>(
                      future: getBookings(false),
                      builder: (context, snap) {
                        if (snap.hasData == false)
                          return CircularProgressIndicator();
                        return Expanded(
                          child: TimetableConfig<CustomCalendarEvent>(
                              dateController: _dateController,
                              timeController: _timeController,
                              eventBuilder: (context, event) =>
                                  BasicEventWidget(event, onTap: () {
                                    showModalBottomSheet(
                                        context: context,
                                        builder: (context) =>
                                            buildBottomNavigation(
                                                context, event.b));
                                  }),
                              child: MultiDateTimetable<CustomCalendarEvent>(),
                              // Optional:
                              eventProvider: eventProviderFromFixedList(snap
                                  .data!
                                  .map((e) => CustomCalendarEvent(e))
                                  .toList()),
                              allDayEventBuilder: (context, event, info) =>
                                  BasicAllDayEventWidget(event, info: info),
                              callbacks: TimetableCallbacks(
                                  onDateTimeBackgroundTap: (DateTime t) async {
                                if (isBooking) return;

                                setState(() {
                                  isBooking = true;
                                });

                                bool result = await res.reserver(context,
                                    start:
                                        t.subtract(Duration(minutes: t.minute)),
                                    end: t
                                        .add(Duration(hours: 1))
                                        .subtract(Duration(minutes: t.minute)));

                                await getBookings(true);

                                setState(() {
                                  isBooking = false;
                                });

                                // display result modal
                                await showOperationResult(
                                    context, "réservée", t, result);
                              }

                                  // onWeekTap, onDateTap, onDateBackgroundTap, onDateTimeBackgroundTap
                                  ),
                              theme: TimetableThemeData(
                                context,
                                // startOfWeek: DateTime.monday,
                                // See the "Theming" section below for more options.
                              )),
                        );
                      }),
            ],
          );
        });
  }

  Widget getValidationText(RentalItemReservation booking, {double size: 9}) {
    return (booking.validated && booking.validatedBy != null)
        ? Padding(
            padding: const EdgeInsets.only(top: 4),
            child: Text(
              "✅ " + (booking.validatedBy?.displayName ?? ""),
              style: TextStyle(
                color: Colors.white,
                fontSize: size,
              ),
              maxLines: 3,
              softWrap: false,
              overflow: TextOverflow.ellipsis,
            ),
          )
        : Padding(
            padding: const EdgeInsets.only(top: 4),
            child: Text(
              "❌ non validé",
              style: TextStyle(
                color: Colors.white,
                fontSize: size,
              ),
              maxLines: 3,
              softWrap: false,
              overflow: TextOverflow.ellipsis,
            ),
          );
  }

  /* Widget appointmentBuilder(
      BuildContext context, CalendarAppointmentDetails details) {
    final Booking? booking = details.appointments.first;

    BookingsDataSource bS = BookingsDataSource(
        details.appointments.map<Booking?>((e) => e).toList());

    if (_controller.view == CalendarView.week) {
      return Container(
        padding: EdgeInsets.all(3),
        alignment: Alignment.topLeft,
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.all(Radius.circular(5)),
          color: bS.getColor(0),
        ),
        child: SingleChildScrollView(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              bS.getSubject(0),
              style: TextStyle(
                color: Colors.white,
                fontSize: 9,
                fontWeight: FontWeight.w500,
              ),
              maxLines: 3,
              softWrap: false,
              overflow: TextOverflow.ellipsis,
            ),
            getValidationText(booking!)
          ],
        )),
      );
    }
    return Container(
      padding: EdgeInsets.all(5),
      alignment: Alignment.topLeft,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.all(Radius.circular(5)),
        color: bS.getColor(0),
      ),
      child: details.bounds.height > 40
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(bS.getSubject(0), style: TextStyle(color: Colors.white)),
                Text("Le : " + booking!.date.toString(),
                    style: TextStyle(color: Colors.white, fontSize: 12)),
                getValidationText(booking, size: 11)
              ],
            )
          : Text(bS.getSubject(0), style: TextStyle(color: Colors.white)),
    );
  }*/

  Future<void> showOperationResult(BuildContext context,
      String messageOperation, DateTime? date, bool result) async {
    if (result) {
      await Flushbar(
        title: "La réservation a bien été " + messageOperation,
        message: "Date : " + date.toString(),
        icon: Icon(Icons.check_circle),
        backgroundColor: Colors.green,
        duration: Duration(seconds: 4),
      ).show(homeContext);
    } else {
      await Flushbar(
        title: "La réservation n'a pas été " + messageOperation,
        message: "Date : " + date.toString(),
        icon: Icon(Icons.error),
        backgroundColor: Colors.red,
        duration: Duration(seconds: 4),
      ).show(homeContext);
    }
  }

  Widget buildBottomNavigation(BuildContext context, RentalItemReservation b) {
    return SizedBox(
        height: 300,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              H1Title("Editer la réservation"),
              Text("Reservation de " +
                  (b.user.displayName ?? b.res.e?.senderId ?? "")),
              CustomElevatedButton(
                  message: "Valider la réservation",
                  icon: Icons.check,
                  color: Colors.green,
                  minimized: false,
                  onPressed: () async {
                    setState(() {
                      isBooking = true;
                    });
                    context.router.pop(context);

                    bool result =
                        await res.validate(b, validated: !b.validated);

                    await getBookings(true);

                    if (mounted)
                      setState(() {
                        isBooking = false;
                      });

                    await showOperationResult(
                        context, "validé", b.date, result);
                  }),
              CustomElevatedButton(
                  message: "Supprimer la réservation",
                  icon: Icons.delete,
                  minimized: false,
                  color: Colors.red,
                  onPressed: () async {
                    setState(() {
                      isBooking = true;
                    });
                    context.router.pop();
                    await b.deleteBooking(context);

                    await getBookings(true);

                    setState(() {
                      isBooking = false;
                    });

                    await showOperationResult(
                        context, "supprimée", b.date, true);
                  }),
            ],
          ),
        ));
  }
}

class CustomCalendarEvent extends t.BasicEvent {
  final RentalItemReservation b;
  CustomCalendarEvent(this.b)
      : super(
          start: b.date!.toUtc(),
          end: b.date!.toUtc().add(Duration(hours: 1)),
          backgroundColor: b.validated == true ? Colors.green : Colors.red,
          id: b.id,
          title: b.validated == true
              ? (b.user.displayName).toString() +
                  " - " +
                  (b.validatedBy?.displayName).toString()
              : b.user.displayName ?? "name",
        ) {}
}
