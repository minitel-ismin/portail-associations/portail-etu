import 'package:auto_route/src/router/auto_router_x.dart';

import 'package:emse_bde_app/logic/matrix/extensions/event_extension.dart';
import 'package:emse_bde_app/logic/models/event/rental_item.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/partials/no_data.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/utils/matrix_widget.dart';

class RentalsPage extends StatefulWidget {
  const RentalsPage({Key? key}) : super(key: key);

  @override
  _RentalsPageState createState() => _RentalsPageState();
}

class _RentalsPageState extends State<RentalsPage> {
  bool creating = false;
  @override
  Widget build(BuildContext context) {
    Client m = Matrix.of(context).client;

    List<RentalItem> items = m.getRentalItems().toList();

    return StreamBuilder<Object>(
        stream: m.onSync.stream,
        builder: (context, snapshot) {
          return ListView(children: [
            CustomHeader("Réservation des salles", actionButton: [
              IconButton(
                  icon:
                      creating ? CircularProgressIndicator() : Icon(Icons.add),
                  onPressed: creating
                      ? null
                      : () async {
                          setState(() {
                            creating = true;
                          });
                          RentalItem? rental = await RentalItem.createNew(m,
                              name: "Nouvelle salle", topic: "");

                          if (rental != null) {
                            context
                                .pushRoute(RentalCreateRoute(rental: rental));
                          }

                          if (mounted) {
                            setState(() {
                              creating = false;
                            });
                          }
                        }),
            ]),
            Column(
              children: [
                if (items.length == 0) NoData(),
                for (RentalItem data in items) buildItemReservation(data)
              ],
            )
          ]);
        });
  }

  Widget buildItemReservation(RentalItem resItem) {
    return ListTile(
        title: Text(resItem.name,
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
        subtitle: Row(
          children: [Text(resItem.description)],
        ),
        trailing: Icon(Icons.navigate_next),
        leading: Padding(
          padding: const EdgeInsets.all(10),
          child: Icon(Icons.emoji_objects, size: 30),
        ),
        onTap: () async {
          context.pushRoute(RentalDetailRoute(reservation: resItem));
        });
  }
}
