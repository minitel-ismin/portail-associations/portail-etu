import 'package:emse_bde_app/logic/matrix/extensions/feed_extension.dart';
import 'package:emse_bde_app/logic/matrix/extensions/portail_extension.dart';
import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:emse_bde_app/logic/models/association_data.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/partials/loading_bar.dart';
import 'package:emse_bde_app/partials/news/feed_item.dart';
import 'package:emse_bde_app/partials/no_data.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/config/matrix_types.dart';
import 'package:minestrix_chat/partials/feed/posts/matrix_post_editor.dart';

class FeedPage extends StatelessWidget {
  const FeedPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: Matrix.of(context)
            .client
            .onSync
            .stream
            .where((up) => up.hasRoomUpdate),
        builder: (context, snapshot) {
          final editableRooms = Matrix.of(context)
              .client
              .getAssoOrEventRooms()
              .where((room) => room.canSendEvent(MatrixTypes.post))
              .toList();
          return ListView(
            children: [
              CustomHeader(
                "Actualitées",
                actionButton: [
                  if (editableRooms.isNotEmpty)
                    IconButton(
                      icon: Icon(Icons.edit),
                      onPressed: () {
                        PostEditorPage.show(
                            rooms: editableRooms, context: context);
                      },
                    )
                ],
              ),
              FeedView()
            ],
          );
        });
  }
}

class FeedView extends StatefulWidget {
  const FeedView({Key? key, this.association: null, this.room})
      : super(key: key);
  final Room? room;
  final AssociationData? association;

  @override
  _FeedViewState createState() => _FeedViewState();
}

class _FeedViewState extends State<FeedView> {
  Future<List<Event>>? messages;
  String? status;

  Future<List<Event>> loadData(Client mClient) async {
    List<Event> f = [];
    if (mClient.prevBatch == null) await mClient.onSync.stream.first;
    await mClient.roomsLoading;
    Logs().level = Level.verbose;
    final startTime = DateTime.now();
    if (widget.room != null) {
      f = await widget.room!.getNews();
    } else {
      for (var i = 0; i < 2; i++) {
        List<Room> rooms;
        String name;
        int pos = 0;

        switch (i) {
          case 0:
            rooms = mClient.getAssoRooms();
            name = "Assos";
            break;
          default:
            name = "Event";
            rooms = mClient.getEventRooms();
        }

        final stopTimeRooms = DateTime.now();
        final elapsedRooms = stopTimeRooms.difference(startTime);
        print("Ellapsed - ${name} ${elapsedRooms.inMilliseconds} ms");
        if (false)
          for (Room r in rooms) {
            f.addAll(await r.getNews());
            setState(() {
              status = name +
                  " - " +
                  r.name +
                  " " +
                  (pos * 100 / rooms.length).toStringAsFixed(0) +
                  "%";
            });
            pos++;
          }
      }
      f.addAll(mClient.rooms.first
          .filterEvents(await mClient.database?.getEventListForType(
                MatrixTypes.post,
                mClient.rooms,
                limit: 60,
              ) ??
              <Event>[]));
      f.sort((a, b) => b.originServerTs.compareTo(a.originServerTs));
    }

    final stopTime = DateTime.now();
    final elapsed = stopTime.difference(startTime);
    print(
        "Ellapsed ${elapsed.inMilliseconds} ms ${f.length} ${elapsed.inMilliseconds / f.length} ms");
    return f;
  }

  bool loading = false;
  Future<bool> loadRooms(Client mClient) async {
    Logs().w("Loads room with set state");
    if (loading == false) {
      if (mounted)
        setState(() {
          loading = true;
        });
      await loadData(mClient);

      if (mounted)
        setState(() {
          loading = false;
        });
      return true;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    final mClient = Matrix.of(context).client;

    if (messages == null) messages = loadData(mClient);
    return FutureBuilder<List<Event>>(
        future: messages,
        builder: (context, snapMessages) {
          if (snapMessages.hasError) return Text("Error");
          if (snapMessages.hasData)
            return Column(
              children: [
                if (snapMessages.data == false)
                  ElevatedButton(
                      child: loading
                          ? LoadingBar("Chargement des rooms")
                          : Text("Charger les rooms"),
                      onPressed: () async {
                        await loadRooms(mClient);
                      }),
                for (Event feedItem in snapMessages.data!)
                  ConstrainedBox(
                      constraints: BoxConstraints(maxWidth: 800),
                      child: FeedItem(feedItem)),
                if (snapMessages.data!.isEmpty)
                  NoData(message: "Pas d'actualitées")
              ],
            );
          return Column(
            children: [
              LoadingBar("Chargement du fil d'actualité"),
              if (status != null) Text(status!)
            ],
          );
        });
  }
}
