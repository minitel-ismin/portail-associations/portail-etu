import 'package:emse_bde_app/pages/welcome/welcome_page.dart';
import 'package:flutter/material.dart';
import 'package:minestrix_chat/view/room_list/room_list_builder.dart';
import 'package:provider/provider.dart';

import 'room_list_wrapper.dart';

class RoomListPage extends StatefulWidget {
  const RoomListPage({Key? key, this.isMobile = false}) : super(key: key);
  final bool isMobile;
  @override
  State<RoomListPage> createState() => _RoomListPageState();
}

class _RoomListPageState extends State<RoomListPage> {
  final scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, _) {
      final state = Provider.of<RoomListWrapperState>(context, listen: false);
      if (state.mobile)
        return Stack(
          children: [
            RoomListBuilder(mobile: true, scrollController: scrollController),
            Positioned(
              left: 10,
              bottom: 10,
              child: Card(
                child: IconButton(
                    icon: const Icon(Icons.chevron_right),
                    onPressed: () {
                      Scaffold.of(context).openDrawer();
                    }),
              ),
            ),
          ],
        );

      return Center(child: WelcomeTitle());
    });
  }
}
