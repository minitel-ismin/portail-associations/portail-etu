import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:flutter/material.dart';
import 'package:minestrix_chat/partials/login/login_card.dart';

class MatrixLoginPage extends StatelessWidget {
  const MatrixLoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final m = Matrix.of(context).client;
    return Scaffold(
      body: Column(
        children: [
          CustomHeader("Matrix login"),
          Expanded(
              child: Center(
                  child: ConstrainedBox(
                      constraints:
                          BoxConstraints(maxWidth: 700, maxHeight: 1200),
                      child: Card(
                          child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: LoginMatrixCard(client: m),
                      )))))
        ],
      ),
    );
  }
}
