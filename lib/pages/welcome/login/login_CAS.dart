import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:emse_bde_app/logic/portail_state.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  final TextEditingController username = TextEditingController();
  final TextEditingController password = TextEditingController();

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String? message;
  bool logging = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                if (Navigator.canPop(context)) context.router.pop();
              }),
        ),
        Center(
          child: Container(
            constraints: BoxConstraints(maxWidth: 500),
            padding: const EdgeInsets.all(12),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text("Authentification Ecole",
                    style: TextStyle(
                        fontSize: 40,
                        fontWeight: FontWeight.w400,
                        letterSpacing: 1.2)),
                SizedBox(height: 50),
                if (message != null)
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 8.0, vertical: 20),
                    child: Text(message!,
                        style: TextStyle(fontSize: 26, color: Colors.red)),
                  ),
                TextField(
                    controller: widget.username,
                    decoration: InputDecoration(
                        labelText: "Nom d'utilisateur",
                        border: const OutlineInputBorder())),
                SizedBox(
                  height: 20,
                ),
                TextField(
                    controller: widget.password,
                    obscureText: true,
                    decoration: InputDecoration(
                        labelText: "Mot de passe",
                        border: const OutlineInputBorder())),
                SizedBox(
                  height: 10,
                ),
                if (logging) LinearProgressIndicator(),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "Connexion",
                        style: TextStyle(fontSize: 23),
                      ),
                    ),
                    onPressed: () async {
                      if (logging == false) {
                        setState(() {
                          logging = true;
                        });
                        try {
                          final p =
                              Provider.of<PortailState>(context, listen: false);

                          bool result = await p.logInCAS(
                              widget.username.text,
                              widget.password.text,
                              "https://matrix.emse.fr/#/");

                          if (result) {
                            context.router.pop();
                          } else {
                            setState(() {
                              message =
                                  "Echec de l'authentification. Veuillez vérifier votre nom d'utilisateur et mot de passe.";
                              widget.password.clear();
                              logging = false;
                            });
                          }
                        } catch (e) {
                          setState(() {
                            message =
                                "Une erreur est survenue, veuillez réessayer...";
                            widget.password.clear();
                            logging = false;
                            print(e.toString());
                          });
                        }
                      }
                    })
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(12),
          child: Text(
              "Ce système vous permet de vous connecter à travers le système d'authentification unique de l'école."),
        )
      ],
    ));
  }
}
