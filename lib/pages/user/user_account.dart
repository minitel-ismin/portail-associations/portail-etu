import 'package:auto_route/src/router/auto_router_x.dart';

import 'package:emse_bde_app/logic/matrix/extensions/portail_extension.dart';
import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:emse_bde_app/logic/portail_state.dart';
import 'package:emse_bde_app/partials/portail_typo.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/partials/matrix/matrix_image_avatar.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';

class UserPage extends StatefulWidget {
  const UserPage({Key? key}) : super(key: key);

  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  bool refreshing = false;
  @override
  Widget build(BuildContext context) {
    final m = Matrix.of(context).client;

    return Consumer<PortailState>(
        builder: (c, state, child) => ListView(
              children: [
                CustomHeader("Mon compte"),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FutureBuilder<ProfileInformation>(
                      future:
                          Matrix.of(context).client.getUserProfile(m.userID!),
                      builder: (context, snap) {
                        return Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 8.0),
                              child: MatrixImageAvatar(
                                  client: Matrix.of(context).client,
                                  height: 120,
                                  width: 120,
                                  defaultText:
                                      snap.data?.displayname ?? m.userID!,
                                  backgroundColor: Colors.blue,
                                  url: snap.data?.avatarUrl,
                                  fit: true),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(28.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(snap.data?.displayname ?? m.userID!,
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.w600)),
                                  if (snap.data?.displayname != m.userID!)
                                    Text(
                                      m.userID!,
                                    ),
                                ],
                              ),
                            ),
                          ],
                        );
                      }),
                ),
                for (Room portail in m.getPortailsRooms())
                  ListTile(
                    title: Text("My portail profile"),
                    trailing: Icon(Icons.arrow_forward),
                    leading: Icon(Icons.person),
                    subtitle: Text(portail.name),
                    onTap: () {
                      context.navigateTo(
                          SettingsPortailProfileRoute(room: portail));
                    },
                  ),
                ListTile(
                  title: Text("Account"),
                  trailing: Icon(Icons.arrow_forward),
                  leading: Icon(Icons.person),
                  onTap: () {
                    context.navigateTo(SettingsAccountRoute());
                  },
                ),
                ListTile(
                  title: Text("Security"),
                  trailing: Icon(Icons.arrow_forward),
                  leading: Icon(Icons.lock),
                  onTap: () {
                    context.navigateTo(SettingsSecurityRoute());
                  },
                ),
                ListTile(
                  title: Text("Theme"),
                  trailing: Icon(Icons.arrow_forward),
                  leading: Icon(Icons.color_lens),
                  onTap: () {
                    context.navigateTo(SettingsThemeRoute());
                  },
                ),
                Padding(
                  padding: const EdgeInsets.all(30),
                  child: Card(
                      child: Padding(
                    padding: const EdgeInsets.all(14),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        PortailH2("Fièrement propulsé par MINITEL"),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 15, horizontal: 8),
                          child:
                              Text("Réalisé par Henri Carnot (@henri2h) EI 20"),
                        )
                      ],
                    ),
                  )),
                ),
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: FutureBuilder<PackageInfo>(
                      future: PackageInfo.fromPlatform(),
                      builder: (context, snap) {
                        if (!snap.hasData) return Container();
                        return Text(
                            "Version : " + (snap.data?.version ?? 'null'));
                      }),
                ),
              ],
            ));
  }
}

class Section extends StatelessWidget {
  const Section({
    Key? key,
    required this.title,
    required this.children,
  }) : super(key: key);

  final String title;
  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [PortailH2(title), for (Widget w in children) w],
      ),
    );
  }
}
