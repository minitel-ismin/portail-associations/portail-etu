import 'package:flutter/material.dart';

import '../../../logic/models/association_data.dart';
import '../../../partials/asso/asso_user_card.dart';

class AssoMandatTab extends StatelessWidget {
  const AssoMandatTab({
    Key? key,
    required this.positions,
  }) : super(key: key);

  final List<Position> positions;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Wrap(
        children: [
          for (Position position in positions
            ..sort((a, b) =>
                a.role?.hierarchy != null && b.role?.hierarchy != null
                    ? b.role!.hierarchy!.compareTo(a.role!.hierarchy!)
                    : 0))
            Padding(
                padding: const EdgeInsets.all(2.0),
                child: AssoUserCard(position: position)),
        ],
      ),
    );
  }
}
