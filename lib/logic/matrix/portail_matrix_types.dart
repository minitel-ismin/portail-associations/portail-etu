class PortailMatrixRoomTypes {
  // rooms
  static const String portail = "fr.emse.minitel.portail";
  static const String asso = "fr.emse.minitel.asso";
  static const String event = "fr.emse.minitel.event";

  static const String rentalItem = "fr.emse.minitel.item.rental";
  static const String storeItem = "fr.emse.minitel.item.store";
}

class PortailMatrixEventTypes {
  // events
  static const String booking = "fr.emse.minitel.booking";
  static const String bookingResponse = "fr.emse.minitel.booking.response";
  static const String bookingResponseValidation =
      "fr.emse.minitel.booking.response.validation";
}

class PortailMatrixStateTypes {
  static const String role = "fr.emse.minitel.role";
  static const String members = "fr.emse.minitel.members";

  static const String form = "fr.emse.minitel.form";

  static const String profile = "fr.emse.minitel.profile";

  static const String event = "fr.emse.minitel.event";
  static const String eventBooking = "fr.emse.minitel.booking";

  static const String eventPollAttendance =
      "fr.emse.minitel.event.attendance_poll";
}
