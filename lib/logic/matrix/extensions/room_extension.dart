import 'package:collection/collection.dart';
import 'package:matrix/matrix.dart';

extension RoomExtension on Room {
  Future<void> joinSpaceChilds() async {
    if (isSpace) {
      for (var space in spaceChildren) {
        bool joined = client.rooms
                .firstWhereOrNull((room) => room.id == space.roomId)
                ?.membership ==
            Membership.join;

        if (joined == false && space.roomId != null) {
          client.joinRoom(space.roomId!, serverName: space.via);
        }
      }
    }
  }
}
