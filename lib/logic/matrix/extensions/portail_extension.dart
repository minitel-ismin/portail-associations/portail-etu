import 'package:emse_bde_app/logic/matrix/extensions/room_extension.dart';
import 'package:emse_bde_app/logic/matrix/portail_matrix_types.dart';
import 'package:emse_bde_app/logic/models/json/user/portailUser.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/minestrix_chat.dart';

extension MatrixPortailExtension on Client {
  Iterable<Room> filterRoomWithType(String type) {
    List<Room> r = rooms.where((r) => r.type == type).toList();
    return r;
  }

  List<Room> getAssoRooms() {
    return rooms
        .where((r) => r.isSpace && r.subtype == PortailMatrixRoomTypes.asso)
        .toList();
  }

  List<Room> getEventRooms() {
    return filterRoomWithType(PortailMatrixRoomTypes.event).toList();
  }

  List<Room> getAssoOrEventRooms() {
    return rooms
        .where((r) =>
            (r.isSpace && r.subtype == PortailMatrixRoomTypes.asso) ||
            r.type == PortailMatrixRoomTypes.event)
        .toList();
  }

  /// Get the collection of association
  List<Room> getPortailsRooms() {
    return rooms
        .where((r) => r.isSpace && r.subtype == PortailMatrixRoomTypes.portail)
        .toList();
  }

  Future<void> joinPortailChilds() async {
    for (Room p in getPortailsRooms()) await p.joinSpaceChilds();
  }

  Future<void> joinAssoChilds() async {
    for (Room asso in getAssoRooms()) await asso.joinSpaceChilds();
  }

  Future<void> joinAssoAndPortailChilds() async {
    Logs().i("Auto joining portail and asso childs");
    await joinPortailChilds();
    await joinAssoChilds();
    Logs().i("Auto join done");
  }

  List<PortailUser> getPortailUsers() {
    List<Room> portails = getPortailsRooms();
    List<PortailUser> users = [];

    for (Room p in portails) {
      List<PortailUser> u = p.getPortailUsers();
      users.addAll(u);
    }

    return users;
  }

  Future<String> createPortail(String name,
      {String? topic, required String spaceAliasName}) async {
    return await createRoom(
        name: name,
        topic: topic,
        visibility: Visibility.private,
        roomAliasName: spaceAliasName,
        creationContent: {
          'type': 'm.space'
        },
        powerLevelContentOverride: {
          'events_default': 100,
          PortailMatrixStateTypes.profile: 0
        },
        initialState: [
          StateEvent(type: "m.room.type", content: {
            "type": PortailMatrixRoomTypes.portail,
          }),
        ]);
  }
}

extension MatrixPortailRoomExtension on Room {
  List<PortailUser> getPortailUsers() {
    List<PortailUser> users = [];

    states[PortailMatrixStateTypes.profile]?.forEach((key, e) {
      if (e.content.isNotEmpty) {
        PortailUser u = PortailUser.fromJson(e.content, key: key);

        users.add(u);
      }
    });
    return users;
  }

  Future<void> setPortailProfile(PortailUser u, {bool shouldWait: true}) async {
    await client.setRoomStateWithKey(
      id,
      PortailMatrixStateTypes.profile,
      u.userID,
      u.toJson(),
    );
    if (shouldWait) await waitForRoomSync();
  }
}
