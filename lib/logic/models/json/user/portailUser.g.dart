// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'portailUser.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PortailUser _$PortailUserFromJson(Map<String, dynamic> json) => PortailUser(
      json['userID'] as String,
    )
      ..about = json['about'] as String?
      ..promo = json['promo'] as String?
      ..birthday = json['birthday'] == null
          ? null
          : DateTime.parse(json['birthday'] as String);

Map<String, dynamic> _$PortailUserToJson(PortailUser instance) =>
    <String, dynamic>{
      'about': instance.about,
      'userID': instance.userID,
      'promo': instance.promo,
      'birthday': instance.birthday?.toIso8601String(),
    };
