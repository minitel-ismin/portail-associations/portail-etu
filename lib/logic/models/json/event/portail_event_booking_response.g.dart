// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'portail_event_booking_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PortailEventBookingResponse _$PortailEventBookingResponseFromJson(
        Map<String, dynamic> json) =>
    PortailEventBookingResponse()
      ..type = $enumDecode(_$ResponseTypeEnumMap, json['type'])
      ..responses = (json['responses'] as Map<String, dynamic>).map(
        (k, e) =>
            MapEntry(k, BookingResponse.fromJson(e as Map<String, dynamic>)),
      )
      ..relates_to = json['relates_to'] == null
          ? null
          : EventRelation.fromJson(json['relates_to'] as Map<String, dynamic>);

Map<String, dynamic> _$PortailEventBookingResponseToJson(
        PortailEventBookingResponse instance) =>
    <String, dynamic>{
      'type': _$ResponseTypeEnumMap[instance.type]!,
      'responses': instance.responses.map((k, e) => MapEntry(k, e.toJson())),
      'relates_to': instance.relates_to?.toJson(),
    };

const _$ResponseTypeEnumMap = {
  ResponseType.going: 'going',
  ResponseType.declined: 'declined',
};
