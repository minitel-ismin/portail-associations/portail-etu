import 'package:emse_bde_app/logic/models/json/event/booking_question.dart';
import 'package:json_annotation/json_annotation.dart';

part 'portail_event_booking.g.dart';

enum PayementMeans { lydia, cash }

@JsonSerializable(explicitToJson: true)

/// A class to store how people can book. It store also the form. (not the bookings)
class PortailEventBooking {
  bool isBookable = false; // whether we can already book this event or not
  List<BookingQuestion> form_questions = [];
  String? currency; // currency used
  int? cost = 0; // base cost of the event

  DateTime? bookingTimeStart; // Bookings before this time won't be accepted.
  DateTime? bookingTimeEnd; // Bookings after this time won't be accepted.

  int? shotgunListLength;
  bool get hasShotgun =>
      (shotgunListLength ?? 0) != 0 && bookingTimeStart != null;

  List<PayementMeans> paymentMeans = [];

  String get displayCost =>
      ((cost ?? 0).toDouble() / 100).toString() +
      " euros"; // set the accepted payment means

  PortailEventBooking();

  factory PortailEventBooking.fromJson(Map<String, dynamic> json) =>
      _$PortailEventBookingFromJson(json);

  Map<String, dynamic> toJson() => _$PortailEventBookingToJson(this);

  String? getErrorMessage() {
    // TODO: configure me
    return null;
  }

  bool get isValid => getErrorMessage() != null;
}
