import 'package:emse_bde_app/logic/models/event/portail_event.dart';
import 'package:emse_bde_app/logic/models/json/event/booking_response.dart';
import 'package:emse_bde_app/logic/models/json/event/matrix_event_int.dart';
import 'package:emse_bde_app/logic/models/json/event/portail_event_booking.dart';
import 'package:emse_bde_app/logic/models/json/matrix/eventRelation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:matrix/matrix.dart';

part 'portail_event_booking_response.g.dart';

enum ResponseType { going, declined }

@JsonSerializable(explicitToJson: true)
class PortailEventBookingResponse extends MatrixEventInt {
  ResponseType type = ResponseType.going;
  Map<String, BookingResponse> responses = {};

  EventRelation? relates_to;

  PortailEventBookingResponse();

  factory PortailEventBookingResponse.fromEvent(Event e) {
    PortailEventBookingResponse res =
        _$PortailEventBookingResponseFromJson(e.content);
    res.e = e;
    return res;
  }

  Map<String, dynamic> toJson() => _$PortailEventBookingResponseToJson(this);

  int getCost(PortailEvent p) {
    PortailEventBooking? b = p.getRelatedEventBooking(this);

    int cost = 0;
    if (b.cost != null) cost = b.cost!;

    // TODO: complete me

    return cost;
  }

  void addResponse(String s, List<String> list) {
    responses[s] = BookingResponse()
      ..id = s
      ..answers = list;
  }
}
