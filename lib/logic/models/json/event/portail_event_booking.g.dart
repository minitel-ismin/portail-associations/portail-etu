// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'portail_event_booking.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PortailEventBooking _$PortailEventBookingFromJson(Map<String, dynamic> json) =>
    PortailEventBooking()
      ..isBookable = json['isBookable'] as bool
      ..form_questions = (json['form_questions'] as List<dynamic>)
          .map((e) => BookingQuestion.fromJson(e as Map<String, dynamic>))
          .toList()
      ..currency = json['currency'] as String?
      ..cost = json['cost'] as int?
      ..bookingTimeStart = json['bookingTimeStart'] == null
          ? null
          : DateTime.parse(json['bookingTimeStart'] as String)
      ..bookingTimeEnd = json['bookingTimeEnd'] == null
          ? null
          : DateTime.parse(json['bookingTimeEnd'] as String)
      ..shotgunListLength = json['shotgunListLength'] as int?
      ..paymentMeans = (json['paymentMeans'] as List<dynamic>)
          .map((e) => $enumDecode(_$PayementMeansEnumMap, e))
          .toList();

Map<String, dynamic> _$PortailEventBookingToJson(
        PortailEventBooking instance) =>
    <String, dynamic>{
      'isBookable': instance.isBookable,
      'form_questions': instance.form_questions.map((e) => e.toJson()).toList(),
      'currency': instance.currency,
      'cost': instance.cost,
      'bookingTimeStart': instance.bookingTimeStart?.toIso8601String(),
      'bookingTimeEnd': instance.bookingTimeEnd?.toIso8601String(),
      'shotgunListLength': instance.shotgunListLength,
      'paymentMeans':
          instance.paymentMeans.map((e) => _$PayementMeansEnumMap[e]!).toList(),
    };

const _$PayementMeansEnumMap = {
  PayementMeans.lydia: 'lydia',
  PayementMeans.cash: 'cash',
};
