import 'package:emse_bde_app/logic/matrix/portail_matrix_types.dart';
import 'package:emse_bde_app/logic/models/base_model.dart';
import 'package:emse_bde_app/logic/models/event/base_portail_event.dart';
import 'package:emse_bde_app/logic/models/event/base_portail_item.dart';
import 'package:emse_bde_app/logic/models/json/event/booking_response.dart';
import 'package:emse_bde_app/logic/models/json/event/portail_event_booking_response.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/minestrix_chat.dart';

class RentalItem extends BasePortailItem {
  RentalItem({required Room r})
      : super(r: r, type: PortailEventTypes.reservation);

  /// Create the matrix event room
  static Future<RentalItem?> createNew(Client c,
      {required String name, required String topic, Room? parentSpace}) async {
    String roomId = await BaseModel.createRoom(c,
        roomType: PortailMatrixRoomTypes.rentalItem,
        name: name,
        topic: topic,
        parentSpace: parentSpace);

    Room? r = c.getRoomById(roomId);
    if (r != null) {
      RentalItem i = RentalItem(r: r);
      return i;
    }
    return null;
  }

  Future<List<RentalItemReservation>> getReservations() async {
    List<PortailEventBookingResponse> responses = await getResponses();
    return responses.map((e) => RentalItemReservation(e, this.t!)).toList();
  }

  Future<bool> reserver(BuildContext context,
      {required DateTime end, required DateTime start}) async {
    PortailEventBookingResponse res = PortailEventBookingResponse();

    res.type = ResponseType.going;
    res.addResponse("time_start", [start.millisecondsSinceEpoch.toString()]);
    res.addResponse("time_emd", [end.millisecondsSinceEpoch.toString()]);

    await r.sendEvent(res.toJson(),
        type: PortailMatrixEventTypes.bookingResponse);
    await r.waitForRoomSync();
    return true;
  }

  Future<bool> validate(RentalItemReservation b,
      {required bool validated}) async {
    BookingResponseValidation val = BookingResponseValidation();
    val.validated = validated;

    await r.sendEvent(val.toJson(),
        type: PortailMatrixEventTypes.bookingResponseValidation,
        inReplyTo: b.res.e);
    await r.waitForRoomSync();
    return true;
  }
}

class RentalItemReservation {
  PortailEventBookingResponse res;
  Timeline t;

  RentalItemReservation(this.res, this.t);

  User get user => res.e!.sender;
  DateTime? get date => res.responses["time_start"]?.answers.isNotEmpty == true
      ? DateTime.fromMillisecondsSinceEpoch(
          int.tryParse(res.responses["time_start"]!.answers[0]) ?? 0)
      : null;

  List<BookingResponseValidation> get validations => res.e!
      .aggregatedEvents(t, RelationshipTypes.reply)
      .where((event) =>
          event.type == PortailMatrixEventTypes.bookingResponseValidation)
      .map((e) => BookingResponseValidation.fromEvent(e))
      .toList();

  bool get validated =>
      validations.isNotEmpty ? validations.last.validated : false;
  User? get validatedBy =>
      validations.isNotEmpty ? validations.last.e?.sender : null;

  String get id => res.e!.eventId;

  Future<String?> deleteBooking(BuildContext context) async {
    return await res.e!.redactEvent();
  }
}
