import 'package:emse_bde_app/logic/matrix/portail_matrix_types.dart';
import 'package:emse_bde_app/logic/models/base_model.dart';
import 'package:emse_bde_app/logic/models/event/base_portail_event.dart';
import 'package:emse_bde_app/logic/models/json/event/booking.dart';
import 'package:emse_bde_app/logic/models/json/event/booking_response.dart';
import 'package:emse_bde_app/logic/models/json/event/portail_event_booking.dart';
import 'package:emse_bde_app/logic/models/json/event/portail_event_booking_response.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/minestrix_chat.dart';
import 'package:minestrix_chat/utils/extensible_event/extensible_event_content.dart';
import 'package:minestrix_chat/utils/poll/json/event_poll_start.dart';
import 'package:minestrix_chat/utils/poll/poll.dart';

class PortailEvent extends BasePortailEvent {
  PortailEvent({required Room r}) : super(r: r, type: PortailEventTypes.event);

  Future<void> saveMatrixEventDescription() async {
    Map<String, dynamic>? body = eventDescription.toJson();
    await r.client
        .setRoomStateWithKey(r.id, PortailMatrixStateTypes.event, "", body);
  }

  /// validate the event and get the error message if there is one
  String? getErrorMessage() {
    if (name == "") {
      if (type == PortailEventTypes.store_item)
        return "Le nom de l'item ne peut pas être nul";

      return "Le nom de l'évènement ne peut pas être nul";
    }
    return null;
  }

  PortailEventBooking getRelatedEventBooking(PortailEventBookingResponse res) {
    // TODO : here we should return the event relation
    return eventBooking;
  }

  /// Create the matrix event room
  static Future<PortailEvent?> createEvent(Client c,
      {required String name, required String topic, Room? parentSpace}) async {
    print("creating event ");

    String roomId = await BaseModel.createRoom(c,
        roomType: PortailMatrixRoomTypes.event,
        name: name,
        topic: topic,
        parentSpace: parentSpace);

    Room? r = c.getRoomById(roomId);
    if (r != null) {
      PortailEvent e = PortailEvent(r: r);

      // Add attendance form
      await e.setPollAttendance();

      return e;
    }
    return null;
  }

  /// Send the poll to get the attendance and update the poll attendance state
  Future<void> setPollAttendance() async {
    EventPollStart pollAttendance = EventPollStart();
    pollAttendance.kind = PollKindEnum.disclosed;
    pollAttendance.answers = [
      PollAnswer()
        ..id = CalendarAttendanceResponses.going
        ..text = "Going",
      PollAnswer()
        ..id = CalendarAttendanceResponses.interested
        ..text = "Interested",
      PollAnswer()
        ..id = CalendarAttendanceResponses.declined
        ..text = "Declined"
    ];

    pollAttendance.question = ExtensibleEventContent()..text = "Viens tu ?";
    pollAttendance.maxSelections = 1;

    String? id = await Poll.sendPollStart(r, pollAttendance);

    if (id != null) {
      await r.client.setRoomStateWithKey(
          r.id, PortailMatrixStateTypes.eventPollAttendance, "", {"id": id});
    }
  }

  /// Try to get the poll attendance event from the timeline or the server
  Future<Event?> getEventAttendanceEvent() async {
    Event? values = r.getState(PortailMatrixStateTypes.eventPollAttendance);
    if (values != null && values.content["id"] != null) {
      Timeline t = await getTimeline();
      return await t.getEventById(values.content["id"]);
    }
    return null;
  }

  /// Bookings

  List<Booking> get bookings => bookingResponses.map((res) {
        PortailEventBooking b = getRelatedEventBooking(res);
        return Booking(booking: b, response: res);
      }).toList();

  List<PortailEventBookingResponse> get bookingResponses =>
      r.states[PortailMatrixEventTypes.bookingResponse]?.values
          .map(((value) => PortailEventBookingResponse.fromEvent(value)))
          .toList() ??
      [];

  /// Book the event. This method send the booking response as a state.
  /// The state key is the user id
  Future<bool> book(Map<String, BookingResponse> list,
      {ResponseType type = ResponseType.going}) async {
    PortailEventBookingResponse res = PortailEventBookingResponse();
    res.type == type;
    res.responses = list;

    await r.client.setRoomStateWithKey(
        r.id,
        PortailMatrixEventTypes.bookingResponse,
        r.client.userID!,
        res.toJson());
    return true;
  }

  int get remainingPlaces =>
      (eventBooking.shotgunListLength ?? 0) - bookingResponses.length;
}
