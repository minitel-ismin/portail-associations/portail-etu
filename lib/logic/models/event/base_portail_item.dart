import 'package:emse_bde_app/logic/matrix/portail_matrix_types.dart';
import 'package:emse_bde_app/logic/models/event/base_portail_event.dart';
import 'package:emse_bde_app/logic/models/json/event/portail_event_booking_response.dart';
import 'package:matrix/matrix.dart';

class BasePortailItem extends BasePortailEvent {
  BasePortailItem({required Room r, PortailEventTypes? type})
      : super(r: r, type: type);

  Future<List<PortailEventBookingResponse>> getResponses() async {
    Timeline t = await getTimeline();

    Iterable<Event> events = t.events
        .where((e) => e.type == PortailMatrixEventTypes.bookingResponse);

    return events.map((e) => PortailEventBookingResponse.fromEvent(e)).toList();
  }
}
