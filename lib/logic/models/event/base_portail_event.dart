import 'package:emse_bde_app/logic/matrix/portail_matrix_types.dart';
import 'package:emse_bde_app/logic/models/base_model.dart';
import 'package:emse_bde_app/logic/models/json/event/portail_event_booking.dart';
import 'package:emse_bde_app/logic/models/json/event/portail_event_description.dart';
import 'package:matrix/matrix.dart';

enum PortailEventTypes { event, reservation, store_item }

class BasePortailEvent extends BaseModel {
  PortailEventTypes? type; // event type : event, reservation, store_item

  PortailEventDescription get eventDescription =>
      r.getState(PortailMatrixStateTypes.event)?.content != null
          ? PortailEventDescription.fromJson(
              r.getState(PortailMatrixStateTypes.event)!.content)
          : PortailEventDescription();

  bool get canSetEventDescription => r.canSendDefaultStates;
  Future<void> setEvenDescription(PortailEventDescription description) async {
    await r.client.setRoomStateWithKey(
        roomId, PortailMatrixStateTypes.event, "", description.toJson());
  }

  PortailEventBooking get eventBooking =>
      r.getState(PortailMatrixStateTypes.eventBooking)?.content != null
          ? PortailEventBooking.fromJson(
              r.getState(PortailMatrixStateTypes.eventBooking)!.content)
          : PortailEventBooking();

  bool get canSetEventBooking => r.canSendDefaultStates;
  Future<void> setEventBooking(PortailEventBooking bookings) async {
    await r.client.setRoomStateWithKey(
        roomId, PortailMatrixStateTypes.eventBooking, "", bookings.toJson());
  }

  BasePortailEvent({required Room r, this.type}) : super(r: r);

  bool get free => eventBooking.cost == null || eventBooking.cost == 0;
  bool get isBookable => eventBooking.isBookable;
  bool get publicEvent => r.joinRules == JoinRules.public;

  DateTime? get date => eventDescription.timeStart;

  Duration get duration => (date != null && eventDescription.timeEnd != null)
      ? eventDescription.timeEnd!.difference(date!)
      : Duration.zero;
}
