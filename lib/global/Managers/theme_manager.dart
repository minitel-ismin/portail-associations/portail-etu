import 'package:emse_bde_app/global/Managers/storage_manager.dart';
import 'package:flutter/material.dart';

enum AppThemeMode { dark, light }

class ThemeNotifier with ChangeNotifier {
  Color get primaryColor => _primaryColor;
  Color _primaryColor = Colors.blue[800]!;

  ThemeData? _darkTheme;
  ThemeData? _lightTheme;

  void _buildTheme() {
    _darkTheme = ThemeData.dark(useMaterial3: true)
        .copyWith(primaryColor: _primaryColor);
    _lightTheme = ThemeData.light(useMaterial3: true)
        .copyWith(primaryColor: _primaryColor);
  }

  ThemeData? _themeData;
  ThemeData? get theme => _themeData;

  AppThemeMode? _mode;
  AppThemeMode? get mode => _mode;

  ThemeNotifier() {
    _loadDataFromStorage();
  }

  Future<void> _loadDataFromStorage() async {
    var color = await StorageManager.readData('color');

    try {
      String valueString =
          color.split('(0x')[1].split(')')[0]; // kind of hacky..
      int value = int.parse(valueString, radix: 16);
      _primaryColor = Color(value);
    } catch (e) {}

    _buildTheme();

    var value = await StorageManager.readData('themeMode');

    print('value read from storage: ' + value.toString());

    _mode = AppThemeMode.values.firstWhere(
        (e) => e.toString() == value.toString(),
        orElse: () => AppThemeMode.light);

    _loadMode();
  }

  void _setMode(AppThemeMode t) {
    StorageManager.saveData('themeMode', t.toString());
    _mode = t;
    _loadMode();
  }

  void _loadMode() {
    switch (_mode) {
      case AppThemeMode.light:
        _themeData = _lightTheme;
        break;

      case AppThemeMode.dark:
        _themeData = _darkTheme;
        break;
      default:
        _themeData = _lightTheme;
    }

    notifyListeners();
  }

  void setDarkMode() async {
    _setMode(AppThemeMode.dark);
  }

  void setLightMode() async {
    _setMode(AppThemeMode.light);
  }

  void setPrimaryColor(Color color) {
    StorageManager.saveData('color', color.toString());
    _primaryColor = color;
    _buildTheme();
    _loadMode();
    notifyListeners();
  }

  static isColorEquals(Color a, Color b) {
    return a.toString().split('(0x')[1].split(')')[0] ==
        b.toString().split('(0x')[1].split(')')[0];
  }
}
